﻿namespace College_Management
{
    partial class TeacherAttendance
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GSearchRecord = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSearchRecord = new System.Windows.Forms.Button();
            this.txtSearchRecrd = new System.Windows.Forms.TextBox();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtAttendance = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.attendance_date = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.txtTeacherName = new System.Windows.Forms.TextBox();
            this.GSearchRecord.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // GSearchRecord
            // 
            this.GSearchRecord.Controls.Add(this.label1);
            this.GSearchRecord.Controls.Add(this.btnSearchRecord);
            this.GSearchRecord.Controls.Add(this.txtSearchRecrd);
            this.GSearchRecord.Location = new System.Drawing.Point(12, 26);
            this.GSearchRecord.Name = "GSearchRecord";
            this.GSearchRecord.Size = new System.Drawing.Size(772, 70);
            this.GSearchRecord.TabIndex = 2;
            this.GSearchRecord.TabStop = false;
            this.GSearchRecord.Text = "[ Search Record ]";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "Reg No.";
            // 
            // btnSearchRecord
            // 
            this.btnSearchRecord.Location = new System.Drawing.Point(650, 26);
            this.btnSearchRecord.Name = "btnSearchRecord";
            this.btnSearchRecord.Size = new System.Drawing.Size(94, 29);
            this.btnSearchRecord.TabIndex = 1;
            this.btnSearchRecord.Text = "&Search";
            this.btnSearchRecord.UseVisualStyleBackColor = true;
            // 
            // txtSearchRecrd
            // 
            this.txtSearchRecrd.Location = new System.Drawing.Point(108, 28);
            this.txtSearchRecrd.Name = "txtSearchRecrd";
            this.txtSearchRecrd.Size = new System.Drawing.Size(518, 27);
            this.txtSearchRecrd.TabIndex = 0;
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(368, 396);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(94, 29);
            this.btnClear.TabIndex = 9;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(234, 396);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(94, 29);
            this.btnAdd.TabIndex = 8;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtAttendance);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.attendance_date);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtTeacherName);
            this.groupBox1.Location = new System.Drawing.Point(12, 116);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(772, 239);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "[ Attendance ]";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(20, 101);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 20);
            this.label2.TabIndex = 47;
            this.label2.Text = "Date";
            // 
            // txtAttendance
            // 
            this.txtAttendance.Location = new System.Drawing.Point(130, 153);
            this.txtAttendance.Name = "txtAttendance";
            this.txtAttendance.Size = new System.Drawing.Size(204, 27);
            this.txtAttendance.TabIndex = 46;
            this.txtAttendance.Text = "Present / Absent";
            this.txtAttendance.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAttendance_KeyPress);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(20, 153);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(85, 20);
            this.label7.TabIndex = 45;
            this.label7.Text = "Attendance";
            // 
            // attendance_date
            // 
            this.attendance_date.Location = new System.Drawing.Point(130, 101);
            this.attendance_date.Name = "attendance_date";
            this.attendance_date.Size = new System.Drawing.Size(263, 27);
            this.attendance_date.TabIndex = 44;
            // 
            // label4
            // 
            this.label4.AutoEllipsis = true;
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(20, 37);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(104, 20);
            this.label4.TabIndex = 6;
            this.label4.Text = "Teacher Name";
            // 
            // txtTeacherName
            // 
            this.txtTeacherName.Location = new System.Drawing.Point(130, 37);
            this.txtTeacherName.Name = "txtTeacherName";
            this.txtTeacherName.Size = new System.Drawing.Size(518, 27);
            this.txtTeacherName.TabIndex = 5;
            this.txtTeacherName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTeacherName_KeyPress);
            // 
            // TeacherAttendance
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.GSearchRecord);
            this.Name = "TeacherAttendance";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TeacherAttendance";
            this.GSearchRecord.ResumeLayout(false);
            this.GSearchRecord.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private GroupBox GSearchRecord;
        private Label label1;
        private Button btnSearchRecord;
        private TextBox txtSearchRecrd;
        private Button btnClear;
        private Button btnAdd;
        private GroupBox groupBox1;
        private Label label2;
        private TextBox txtAttendance;
        private Label label7;
        private DateTimePicker attendance_date;
        private Label label4;
        private TextBox txtTeacherName;
    }
}