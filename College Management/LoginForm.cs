﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace College_Management
{
    public partial class LoginForm : Form
    {
        public LoginForm()
        {
            InitializeComponent();
        }

        private Boolean valid()
        {
            if(txtUsername.Text == "")
            {
                MessageBox.Show("Enter Username");
                txtUsername.Focus();
                return false;
            }
            if(txtPassword.Text == "")
            {
                MessageBox.Show("Enter Password");
                txtPassword.Focus();
                return false;
            }
            return true;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            if (valid())
            {
                if(txtUsername.Text == "Admin" && txtPassword.Text == "Admin")
                {
                    Dashboard d = new Dashboard();
                    d.ShowDialog();
                }
                else
                {
                    MessageBox.Show("Username or Password is invalid");
                }
            }
        }
    }
}
