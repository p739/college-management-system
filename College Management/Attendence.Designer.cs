﻿namespace College_Management
{
    partial class Attendence
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GSearchRecord = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSearchRecord = new System.Windows.Forms.Button();
            this.txtSearchRecrd = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.attendance_date = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtRollNo = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtStudentName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtSemister = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtCource = new System.Windows.Forms.TextBox();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.txtAttendance = new System.Windows.Forms.TextBox();
            this.GSearchRecord.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // GSearchRecord
            // 
            this.GSearchRecord.Controls.Add(this.label1);
            this.GSearchRecord.Controls.Add(this.btnSearchRecord);
            this.GSearchRecord.Controls.Add(this.txtSearchRecrd);
            this.GSearchRecord.Location = new System.Drawing.Point(12, 12);
            this.GSearchRecord.Name = "GSearchRecord";
            this.GSearchRecord.Size = new System.Drawing.Size(772, 70);
            this.GSearchRecord.TabIndex = 1;
            this.GSearchRecord.TabStop = false;
            this.GSearchRecord.Text = "[ Search Record ]";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "PRN No.";
            // 
            // btnSearchRecord
            // 
            this.btnSearchRecord.Location = new System.Drawing.Point(650, 26);
            this.btnSearchRecord.Name = "btnSearchRecord";
            this.btnSearchRecord.Size = new System.Drawing.Size(94, 29);
            this.btnSearchRecord.TabIndex = 1;
            this.btnSearchRecord.Text = "&Search";
            this.btnSearchRecord.UseVisualStyleBackColor = true;
            // 
            // txtSearchRecrd
            // 
            this.txtSearchRecrd.Location = new System.Drawing.Point(108, 28);
            this.txtSearchRecrd.Name = "txtSearchRecrd";
            this.txtSearchRecrd.Size = new System.Drawing.Size(518, 27);
            this.txtSearchRecrd.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtAttendance);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.attendance_date);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txtRollNo);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtStudentName);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtSemister);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtCource);
            this.groupBox1.Location = new System.Drawing.Point(12, 101);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(772, 239);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "[ Attendance ]";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(20, 179);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(85, 20);
            this.label7.TabIndex = 45;
            this.label7.Text = "Attendance";
            // 
            // attendance_date
            // 
            this.attendance_date.Location = new System.Drawing.Point(416, 127);
            this.attendance_date.Name = "attendance_date";
            this.attendance_date.Size = new System.Drawing.Size(263, 27);
            this.attendance_date.TabIndex = 44;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(356, 127);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 20);
            this.label6.TabIndex = 10;
            this.label6.Text = "Date";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(20, 124);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 20);
            this.label5.TabIndex = 8;
            this.label5.Text = "Roll No.";
            // 
            // txtRollNo
            // 
            this.txtRollNo.Location = new System.Drawing.Point(130, 124);
            this.txtRollNo.Name = "txtRollNo";
            this.txtRollNo.Size = new System.Drawing.Size(204, 27);
            this.txtRollNo.TabIndex = 7;
            this.txtRollNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtRollNo_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoEllipsis = true;
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(20, 78);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(104, 20);
            this.label4.TabIndex = 6;
            this.label4.Text = "Student Name";
            // 
            // txtStudentName
            // 
            this.txtStudentName.Location = new System.Drawing.Point(130, 78);
            this.txtStudentName.Name = "txtStudentName";
            this.txtStudentName.Size = new System.Drawing.Size(518, 27);
            this.txtStudentName.TabIndex = 5;
            this.txtStudentName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtStudentName_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(356, 34);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 20);
            this.label3.TabIndex = 4;
            this.label3.Text = "Semister";
            // 
            // txtSemister
            // 
            this.txtSemister.Location = new System.Drawing.Point(444, 31);
            this.txtSemister.Name = "txtSemister";
            this.txtSemister.Size = new System.Drawing.Size(204, 27);
            this.txtSemister.TabIndex = 3;
            this.txtSemister.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSemister_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(20, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 20);
            this.label2.TabIndex = 2;
            this.label2.Text = "Cource";
            // 
            // txtCource
            // 
            this.txtCource.Location = new System.Drawing.Point(130, 28);
            this.txtCource.Name = "txtCource";
            this.txtCource.Size = new System.Drawing.Size(204, 27);
            this.txtCource.TabIndex = 0;
            this.txtCource.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCource_KeyPress);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(234, 381);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(94, 29);
            this.btnAdd.TabIndex = 4;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(368, 381);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(94, 29);
            this.btnClear.TabIndex = 6;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // txtAttendance
            // 
            this.txtAttendance.Location = new System.Drawing.Point(130, 179);
            this.txtAttendance.Name = "txtAttendance";
            this.txtAttendance.Size = new System.Drawing.Size(204, 27);
            this.txtAttendance.TabIndex = 46;
            this.txtAttendance.Text = "Present / Absent";
            this.txtAttendance.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAttendance_KeyPress);
            // 
            // Attendence
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(807, 467);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.GSearchRecord);
            this.Name = "Attendence";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Attendence";
            this.GSearchRecord.ResumeLayout(false);
            this.GSearchRecord.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private GroupBox GSearchRecord;
        private Label label1;
        private Button btnSearchRecord;
        private TextBox txtSearchRecrd;
        private GroupBox groupBox1;
        private Label label6;
        private Label label5;
        private TextBox txtRollNo;
        private Label label4;
        private TextBox txtStudentName;
        private Label label3;
        private TextBox txtSemister;
        private Label label2;
        private TextBox txtCource;
        private Label label7;
        private DateTimePicker attendance_date;
        private Button btnAdd;
        private Button btnClear;
        private TextBox txtAttendance;
    }
}