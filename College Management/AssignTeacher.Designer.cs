﻿namespace College_Management
{
    partial class AssignTeacher
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.assign_date = new System.Windows.Forms.DateTimePicker();
            this.date_of_birth = new System.Windows.Forms.DateTimePicker();
            this.txtshift = new System.Windows.Forms.TextBox();
            this.txtSepci = new System.Windows.Forms.TextBox();
            this.txtExperience = new System.Windows.Forms.TextBox();
            this.txtGender = new System.Windows.Forms.TextBox();
            this.txtRegNo = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.txtPhonNo = new System.Windows.Forms.TextBox();
            this.txtTeacherName = new System.Windows.Forms.TextBox();
            this.txtTeacherAddr = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.btnStudPicUpload = new System.Windows.Forms.Button();
            this.pboxstudentupload = new System.Windows.Forms.PictureBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSearchRecord = new System.Windows.Forms.Button();
            this.txtSearchRecrd = new System.Windows.Forms.TextBox();
            this.GAddStudent = new System.Windows.Forms.GroupBox();
            this.GSearchRecord = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.pboxstudentupload)).BeginInit();
            this.GAddStudent.SuspendLayout();
            this.GSearchRecord.SuspendLayout();
            this.SuspendLayout();
            // 
            // assign_date
            // 
            this.assign_date.Location = new System.Drawing.Point(134, 206);
            this.assign_date.Name = "assign_date";
            this.assign_date.Size = new System.Drawing.Size(263, 27);
            this.assign_date.TabIndex = 43;
            // 
            // date_of_birth
            // 
            this.date_of_birth.Location = new System.Drawing.Point(133, 169);
            this.date_of_birth.Name = "date_of_birth";
            this.date_of_birth.Size = new System.Drawing.Size(264, 27);
            this.date_of_birth.TabIndex = 42;
            // 
            // txtshift
            // 
            this.txtshift.Location = new System.Drawing.Point(131, 386);
            this.txtshift.Name = "txtshift";
            this.txtshift.Size = new System.Drawing.Size(254, 27);
            this.txtshift.TabIndex = 41;
            this.txtshift.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtshift_KeyPress);
            // 
            // txtSepci
            // 
            this.txtSepci.Location = new System.Drawing.Point(133, 346);
            this.txtSepci.Name = "txtSepci";
            this.txtSepci.Size = new System.Drawing.Size(385, 27);
            this.txtSepci.TabIndex = 39;
            this.txtSepci.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSepci_KeyPress);
            // 
            // txtExperience
            // 
            this.txtExperience.Location = new System.Drawing.Point(134, 313);
            this.txtExperience.Name = "txtExperience";
            this.txtExperience.Size = new System.Drawing.Size(385, 27);
            this.txtExperience.TabIndex = 38;
            this.txtExperience.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtExperience_KeyPress);
            // 
            // txtGender
            // 
            this.txtGender.Location = new System.Drawing.Point(134, 100);
            this.txtGender.Name = "txtGender";
            this.txtGender.Size = new System.Drawing.Size(385, 27);
            this.txtGender.TabIndex = 37;
            this.txtGender.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtGender_KeyPress);
            // 
            // txtRegNo
            // 
            this.txtRegNo.Location = new System.Drawing.Point(134, 25);
            this.txtRegNo.Name = "txtRegNo";
            this.txtRegNo.Size = new System.Drawing.Size(385, 27);
            this.txtRegNo.TabIndex = 36;
            this.txtRegNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtRegNo_KeyPress);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(14, 32);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(94, 20);
            this.label14.TabIndex = 35;
            this.label14.Text = "Regester No.";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(675, 85);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(91, 20);
            this.label13.TabIndex = 34;
            this.label13.Text = "Insert Image";
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(558, 422);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(94, 29);
            this.btnClose.TabIndex = 33;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(439, 422);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(94, 29);
            this.btnClear.TabIndex = 32;
            this.btnClear.Text = "&Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(320, 422);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(94, 29);
            this.btnUpdate.TabIndex = 31;
            this.btnUpdate.Text = "&Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(201, 422);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(94, 29);
            this.btnSave.TabIndex = 30;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(133, 276);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(385, 27);
            this.txtEmail.TabIndex = 21;
            // 
            // txtPhonNo
            // 
            this.txtPhonNo.Location = new System.Drawing.Point(133, 239);
            this.txtPhonNo.Name = "txtPhonNo";
            this.txtPhonNo.Size = new System.Drawing.Size(385, 27);
            this.txtPhonNo.TabIndex = 20;
            this.txtPhonNo.TextChanged += new System.EventHandler(this.txtPhonNo_TextChanged);
            this.txtPhonNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPhonNo_KeyPress);
            // 
            // txtTeacherName
            // 
            this.txtTeacherName.Location = new System.Drawing.Point(133, 58);
            this.txtTeacherName.Name = "txtTeacherName";
            this.txtTeacherName.Size = new System.Drawing.Size(385, 27);
            this.txtTeacherName.TabIndex = 19;
            this.txtTeacherName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTeacherName_KeyPress);
            // 
            // txtTeacherAddr
            // 
            this.txtTeacherAddr.Location = new System.Drawing.Point(133, 133);
            this.txtTeacherAddr.Name = "txtTeacherAddr";
            this.txtTeacherAddr.Size = new System.Drawing.Size(385, 27);
            this.txtTeacherAddr.TabIndex = 17;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(20, 393);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(39, 20);
            this.label11.TabIndex = 14;
            this.label11.Text = "Shift";
            // 
            // btnStudPicUpload
            // 
            this.btnStudPicUpload.Location = new System.Drawing.Point(677, 301);
            this.btnStudPicUpload.Name = "btnStudPicUpload";
            this.btnStudPicUpload.Size = new System.Drawing.Size(94, 29);
            this.btnStudPicUpload.TabIndex = 13;
            this.btnStudPicUpload.Text = "&Upload";
            this.btnStudPicUpload.UseVisualStyleBackColor = true;
            // 
            // pboxstudentupload
            // 
            this.pboxstudentupload.Location = new System.Drawing.Point(632, 116);
            this.pboxstudentupload.Name = "pboxstudentupload";
            this.pboxstudentupload.Size = new System.Drawing.Size(181, 179);
            this.pboxstudentupload.TabIndex = 12;
            this.pboxstudentupload.TabStop = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(13, 353);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(102, 20);
            this.label10.TabIndex = 11;
            this.label10.Text = "Specialization";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(13, 316);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(81, 20);
            this.label9.TabIndex = 10;
            this.label9.Text = "Experience";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(13, 279);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(46, 20);
            this.label8.TabIndex = 9;
            this.label8.Text = "Email";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(13, 242);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(77, 20);
            this.label7.TabIndex = 8;
            this.label7.Text = "Phone No.";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(13, 205);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(88, 20);
            this.label6.TabIndex = 7;
            this.label6.Text = "Assign Date";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 168);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(96, 20);
            this.label5.TabIndex = 6;
            this.label5.Text = "Date Of Birth";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 136);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(62, 20);
            this.label4.TabIndex = 5;
            this.label4.Text = "Address";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(14, 103);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "Gender";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 61);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Teacher Name";
            // 
            // btnSearchRecord
            // 
            this.btnSearchRecord.Location = new System.Drawing.Point(608, 30);
            this.btnSearchRecord.Name = "btnSearchRecord";
            this.btnSearchRecord.Size = new System.Drawing.Size(94, 29);
            this.btnSearchRecord.TabIndex = 1;
            this.btnSearchRecord.Text = "&Search";
            this.btnSearchRecord.UseVisualStyleBackColor = true;
            // 
            // txtSearchRecrd
            // 
            this.txtSearchRecrd.Location = new System.Drawing.Point(57, 26);
            this.txtSearchRecrd.Name = "txtSearchRecrd";
            this.txtSearchRecrd.Size = new System.Drawing.Size(518, 27);
            this.txtSearchRecrd.TabIndex = 0;
            // 
            // GAddStudent
            // 
            this.GAddStudent.Controls.Add(this.assign_date);
            this.GAddStudent.Controls.Add(this.date_of_birth);
            this.GAddStudent.Controls.Add(this.txtshift);
            this.GAddStudent.Controls.Add(this.txtSepci);
            this.GAddStudent.Controls.Add(this.txtExperience);
            this.GAddStudent.Controls.Add(this.txtGender);
            this.GAddStudent.Controls.Add(this.txtRegNo);
            this.GAddStudent.Controls.Add(this.label14);
            this.GAddStudent.Controls.Add(this.label13);
            this.GAddStudent.Controls.Add(this.btnClose);
            this.GAddStudent.Controls.Add(this.btnClear);
            this.GAddStudent.Controls.Add(this.btnUpdate);
            this.GAddStudent.Controls.Add(this.btnSave);
            this.GAddStudent.Controls.Add(this.txtEmail);
            this.GAddStudent.Controls.Add(this.txtPhonNo);
            this.GAddStudent.Controls.Add(this.txtTeacherName);
            this.GAddStudent.Controls.Add(this.txtTeacherAddr);
            this.GAddStudent.Controls.Add(this.label11);
            this.GAddStudent.Controls.Add(this.btnStudPicUpload);
            this.GAddStudent.Controls.Add(this.pboxstudentupload);
            this.GAddStudent.Controls.Add(this.label10);
            this.GAddStudent.Controls.Add(this.label9);
            this.GAddStudent.Controls.Add(this.label8);
            this.GAddStudent.Controls.Add(this.label7);
            this.GAddStudent.Controls.Add(this.label6);
            this.GAddStudent.Controls.Add(this.label5);
            this.GAddStudent.Controls.Add(this.label4);
            this.GAddStudent.Controls.Add(this.label3);
            this.GAddStudent.Controls.Add(this.label1);
            this.GAddStudent.Location = new System.Drawing.Point(12, 113);
            this.GAddStudent.Name = "GAddStudent";
            this.GAddStudent.Size = new System.Drawing.Size(935, 475);
            this.GAddStudent.TabIndex = 3;
            this.GAddStudent.TabStop = false;
            this.GAddStudent.Text = "[ Student Admission Details ]";
            // 
            // GSearchRecord
            // 
            this.GSearchRecord.Controls.Add(this.btnSearchRecord);
            this.GSearchRecord.Controls.Add(this.txtSearchRecrd);
            this.GSearchRecord.Location = new System.Drawing.Point(6, 20);
            this.GSearchRecord.Name = "GSearchRecord";
            this.GSearchRecord.Size = new System.Drawing.Size(772, 70);
            this.GSearchRecord.TabIndex = 2;
            this.GSearchRecord.TabStop = false;
            this.GSearchRecord.Text = "[ Search Record ]";
            // 
            // AssignTeacher
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(967, 600);
            this.Controls.Add(this.GAddStudent);
            this.Controls.Add(this.GSearchRecord);
            this.Name = "AssignTeacher";
            this.Text = "AssignTeacher";
            //this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.AssignTeacher_KeyPress);
            ((System.ComponentModel.ISupportInitialize)(this.pboxstudentupload)).EndInit();
            this.GAddStudent.ResumeLayout(false);
            this.GAddStudent.PerformLayout();
            this.GSearchRecord.ResumeLayout(false);
            this.GSearchRecord.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DateTimePicker assign_date;
        private DateTimePicker date_of_birth;
        private TextBox txtshift;
        private TextBox txtExperience;
        private TextBox txtGender;
        private TextBox txtRegNo;
        private Label label14;
        private Label label13;
        private Button btnClose;
        private Button btnClear;
        private Button btnUpdate;
        private Button btnSave;
        private TextBox txtEmail;
        private TextBox txtPhonNo;
        private TextBox txtTeacherName;
        private TextBox txtTeacherAddr;
        private Label label11;
        private Button btnStudPicUpload;
        private PictureBox pboxstudentupload;
        private Label label10;
        private Label label9;
        private Label label8;
        private Label label7;
        private Label label6;
        private Label label5;
        private Label label4;
        private Label label3;
        private Label label1;
        private Button btnSearchRecord;
        private TextBox txtSearchRecrd;
        private GroupBox GAddStudent;
        private GroupBox GSearchRecord;
        private TextBox txtSepci;
    }
}