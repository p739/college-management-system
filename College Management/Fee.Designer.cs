﻿namespace College_Management
{
    partial class Fee
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.studFee = new System.Windows.Forms.GroupBox();
            this.btnFeeUpdate = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnSearchPRN = new System.Windows.Forms.Button();
            this.txtSearchPRN = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.studDetail = new System.Windows.Forms.GroupBox();
            this.Paid_Date_fee = new System.Windows.Forms.DateTimePicker();
            this.txtDueAmount = new System.Windows.Forms.TextBox();
            this.txtAmount = new System.Windows.Forms.TextBox();
            this.txtSemister = new System.Windows.Forms.TextBox();
            this.txtPaidAmount = new System.Windows.Forms.TextBox();
            this.txtCatagory = new System.Windows.Forms.TextBox();
            this.txtCource = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtRollNo = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtStudName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.studFee.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.studDetail.SuspendLayout();
            this.SuspendLayout();
            // 
            // studFee
            // 
            this.studFee.Controls.Add(this.btnFeeUpdate);
            this.studFee.Controls.Add(this.groupBox1);
            this.studFee.Controls.Add(this.btnClear);
            this.studFee.Controls.Add(this.btnAdd);
            this.studFee.Controls.Add(this.studDetail);
            this.studFee.Location = new System.Drawing.Point(11, 12);
            this.studFee.Name = "studFee";
            this.studFee.Size = new System.Drawing.Size(1031, 478);
            this.studFee.TabIndex = 0;
            this.studFee.TabStop = false;
            this.studFee.Text = "[ Student Fee Details ]";
            // 
            // btnFeeUpdate
            // 
            this.btnFeeUpdate.Location = new System.Drawing.Point(471, 425);
            this.btnFeeUpdate.Name = "btnFeeUpdate";
            this.btnFeeUpdate.Size = new System.Drawing.Size(94, 29);
            this.btnFeeUpdate.TabIndex = 5;
            this.btnFeeUpdate.Text = "&Update";
            this.btnFeeUpdate.UseVisualStyleBackColor = true;
            this.btnFeeUpdate.Click += new System.EventHandler(this.btnFeeUpdate_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnSearchPRN);
            this.groupBox1.Controls.Add(this.txtSearchPRN);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(22, 40);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(990, 89);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "[ Search ]";
            // 
            // btnSearchPRN
            // 
            this.btnSearchPRN.Location = new System.Drawing.Point(818, 41);
            this.btnSearchPRN.Name = "btnSearchPRN";
            this.btnSearchPRN.Size = new System.Drawing.Size(94, 29);
            this.btnSearchPRN.TabIndex = 5;
            this.btnSearchPRN.Text = "&Search";
            this.btnSearchPRN.UseVisualStyleBackColor = true;
            // 
            // txtSearchPRN
            // 
            this.txtSearchPRN.Location = new System.Drawing.Point(124, 43);
            this.txtSearchPRN.Name = "txtSearchPRN";
            this.txtSearchPRN.Size = new System.Drawing.Size(668, 27);
            this.txtSearchPRN.TabIndex = 18;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "PRN No.";
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(590, 425);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(94, 29);
            this.btnClear.TabIndex = 3;
            this.btnClear.Text = "&Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(352, 425);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(94, 29);
            this.btnAdd.TabIndex = 2;
            this.btnAdd.Text = "&Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // studDetail
            // 
            this.studDetail.Controls.Add(this.Paid_Date_fee);
            this.studDetail.Controls.Add(this.txtDueAmount);
            this.studDetail.Controls.Add(this.txtAmount);
            this.studDetail.Controls.Add(this.txtSemister);
            this.studDetail.Controls.Add(this.txtPaidAmount);
            this.studDetail.Controls.Add(this.txtCatagory);
            this.studDetail.Controls.Add(this.txtCource);
            this.studDetail.Controls.Add(this.label10);
            this.studDetail.Controls.Add(this.label9);
            this.studDetail.Controls.Add(this.label8);
            this.studDetail.Controls.Add(this.label7);
            this.studDetail.Controls.Add(this.label6);
            this.studDetail.Controls.Add(this.label5);
            this.studDetail.Controls.Add(this.label4);
            this.studDetail.Controls.Add(this.txtRollNo);
            this.studDetail.Controls.Add(this.label3);
            this.studDetail.Controls.Add(this.txtStudName);
            this.studDetail.Controls.Add(this.label2);
            this.studDetail.Location = new System.Drawing.Point(22, 135);
            this.studDetail.Name = "studDetail";
            this.studDetail.Size = new System.Drawing.Size(990, 269);
            this.studDetail.TabIndex = 1;
            this.studDetail.TabStop = false;
            this.studDetail.Text = "[ Student Details ]";
            // 
            // Paid_Date_fee
            // 
            this.Paid_Date_fee.Location = new System.Drawing.Point(137, 220);
            this.Paid_Date_fee.Name = "Paid_Date_fee";
            this.Paid_Date_fee.Size = new System.Drawing.Size(250, 27);
            this.Paid_Date_fee.TabIndex = 18;
            // 
            // txtDueAmount
            // 
            this.txtDueAmount.Location = new System.Drawing.Point(544, 157);
            this.txtDueAmount.Name = "txtDueAmount";
            this.txtDueAmount.Size = new System.Drawing.Size(257, 27);
            this.txtDueAmount.TabIndex = 17;
            this.txtDueAmount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDueAmount_KeyPress);
            // 
            // txtAmount
            // 
            this.txtAmount.Location = new System.Drawing.Point(544, 118);
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.Size = new System.Drawing.Size(257, 27);
            this.txtAmount.TabIndex = 16;
            this.txtAmount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAmount_KeyPress);
            // 
            // txtSemister
            // 
            this.txtSemister.Location = new System.Drawing.Point(544, 80);
            this.txtSemister.Name = "txtSemister";
            this.txtSemister.Size = new System.Drawing.Size(257, 27);
            this.txtSemister.TabIndex = 15;
            this.txtSemister.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSemister_KeyPress);
            // 
            // txtPaidAmount
            // 
            this.txtPaidAmount.Location = new System.Drawing.Point(133, 170);
            this.txtPaidAmount.Name = "txtPaidAmount";
            this.txtPaidAmount.Size = new System.Drawing.Size(257, 27);
            this.txtPaidAmount.TabIndex = 13;
            this.txtPaidAmount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPaidAmount_KeyPress);
            // 
            // txtCatagory
            // 
            this.txtCatagory.Location = new System.Drawing.Point(133, 125);
            this.txtCatagory.Name = "txtCatagory";
            this.txtCatagory.Size = new System.Drawing.Size(257, 27);
            this.txtCatagory.TabIndex = 12;
            this.txtCatagory.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCatagory_KeyPress);
            // 
            // txtCource
            // 
            this.txtCource.Location = new System.Drawing.Point(133, 77);
            this.txtCource.Name = "txtCource";
            this.txtCource.Size = new System.Drawing.Size(257, 27);
            this.txtCource.TabIndex = 11;
            this.txtCource.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCource_KeyPress);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(22, 215);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(73, 20);
            this.label10.TabIndex = 10;
            this.label10.Text = "Paid Date";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(445, 157);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(93, 20);
            this.label9.TabIndex = 9;
            this.label9.Text = "Due Amount";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(22, 170);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(94, 20);
            this.label8.TabIndex = 8;
            this.label8.Text = "Paid Amount";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(22, 125);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(69, 20);
            this.label7.TabIndex = 7;
            this.label7.Text = "Catagory";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(445, 117);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(62, 20);
            this.label6.TabIndex = 6;
            this.label6.Text = "Amount";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(445, 77);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(66, 20);
            this.label5.TabIndex = 5;
            this.label5.Text = "Semister";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(22, 80);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 20);
            this.label4.TabIndex = 4;
            this.label4.Text = "Cource";
            // 
            // txtRollNo
            // 
            this.txtRollNo.Location = new System.Drawing.Point(544, 37);
            this.txtRollNo.Name = "txtRollNo";
            this.txtRollNo.Size = new System.Drawing.Size(257, 27);
            this.txtRollNo.TabIndex = 3;
            this.txtRollNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtRollNo_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(445, 37);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "Roll No.";
            // 
            // txtStudName
            // 
            this.txtStudName.Location = new System.Drawing.Point(133, 37);
            this.txtStudName.Name = "txtStudName";
            this.txtStudName.Size = new System.Drawing.Size(257, 27);
            this.txtStudName.TabIndex = 1;
            this.txtStudName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtStudName_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(104, 20);
            this.label2.TabIndex = 0;
            this.label2.Text = "Student Name";
            // 
            // Fee
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1054, 502);
            this.Controls.Add(this.studFee);
            this.Name = "Fee";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Fee";
            this.Load += new System.EventHandler(this.Fee_Load);
            this.studFee.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.studDetail.ResumeLayout(false);
            this.studDetail.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private GroupBox studFee;
        private GroupBox studDetail;
        private Label label2;
        private Button btnClear;
        private Button btnAdd;
        private TextBox txtDueAmount;
        private TextBox txtAmount;
        private TextBox txtSemister;
        private TextBox txtPaidAmount;
        private TextBox txtCatagory;
        private TextBox txtCource;
        private Label label10;
        private Label label9;
        private Label label8;
        private Label label7;
        private Label label6;
        private Label label5;
        private Label label4;
        private TextBox txtRollNo;
        private Label label3;
        private TextBox txtStudName;
        private GroupBox groupBox1;
        private Button btnSearchPRN;
        private TextBox txtSearchPRN;
        private Label label1;
        private Button btnFeeUpdate;
        private DateTimePicker Paid_Date_fee;
    }
}