﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//using System.Windows.Forms.TextBox;
using System.Data.SqlClient;

namespace College_Management
{
    public partial class StudentAdmission : Form
    {
        public StudentAdmission()
        {
            InitializeComponent();
        }

        private void StudentAdmission_Load(object sender, EventArgs e)
        {

        }

        private Boolean valid()
        {
            if(txtStudName.Text =="")
            {
                MessageBox.Show("Please Enter Student Name");
                txtStudName.Focus();
                return false;
            }
            if(txtStudRno.Text =="")
            {
                MessageBox.Show("Please Enter Student Roll No.");
                txtStudRno.Focus();
                return false;
            }
            if(txtGender.Text == "")    
            {
                MessageBox.Show("Please Enter Gender");
                txtGender.Focus();
                return false;
            }
            if(txtStudAddr.Text =="")
            {
                MessageBox.Show("Please Enter Student Address");
                txtStudAddr.Focus();
                return false;
            }
            if(txtPhonNo.Text == "")
            {
                MessageBox.Show("Please Enter Student Mobile Number");
                txtPhonNo.Focus();
                return false;
            }
            if(txtEmail.Text == "")
            {
                MessageBox.Show("Please Enter Student Email ID");
                txtEmail.Focus();
                return false;
            }
            return true;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (valid())
            {
                //connection_string
                SqlConnection con = new SqlConnection(@"Data Source=BLACKTECH;Initial Catalog=CollegeManagement;Integrated Security=True");
                
                try
                {
                    // #Logic : for_fordwording_data_from_txtbox_to_database_by_procedure
                    SqlCommand cmd = new SqlCommand();

                    if(txtPrnNo.Text == "")
                    {
                        cmd.Parameters.AddWithValue("@PRN_NO", null);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@PRN_NO", txtPrnNo.Text);
                    }
                    cmd.Parameters.AddWithValue("@studname", txtStudName.Text);
                    cmd.Parameters.AddWithValue("@studrno", txtStudRno.Text);
                    cmd.Parameters.AddWithValue("@gender", txtGender.Text);
                    cmd.Parameters.AddWithValue("@address", txtStudAddr.Text);
                    cmd.Parameters.AddWithValue("@DOB", date_of_birth.Text.ToString());
                    cmd.Parameters.AddWithValue("@DA", admission_date.Text.ToString());
                    cmd.Parameters.AddWithValue("@phoneno", txtPhonNo.Text);
                    cmd.Parameters.AddWithValue("@emial", txtEmail.Text);
                    cmd.Parameters.AddWithValue("@counrty", txtcountry.Text);
                    cmd.Parameters.AddWithValue("@course", txtcource.Text);
                    cmd.Parameters.AddWithValue("@shift", txtshift.Text);
                    cmd.Parameters.AddWithValue("@semister", txtsem.Text);

                    //name _of_procedure_prescent_in_the_database
                    cmd.CommandText = "p_Student_Admission";
                    cmd.CommandType = CommandType.StoredProcedure;

                    //getting_return_value_when_procedure_is_executed
                    SqlParameter rvalue = cmd.Parameters.Add("@r", SqlDbType.Int);
                    rvalue.Direction = ParameterDirection.ReturnValue;

                    //Execution_of_procedure
                    cmd.Connection = con;
                    con.Open();
                    cmd.ExecuteNonQuery();

                    int r = 0;
                    r = (int)rvalue.Value;

                    //for_success_the_data_enty_into_database
                    if (r > 0)
                    {
                        MessageBox.Show("Data Saved Successfully");

                        //#clearing_data_prescent_in_the_txtboxes
                        txtPrnNo.Clear();
                        txtGender.Clear();
                        txtcountry.Clear();
                        txtcource.Clear();
                        txtsem.Clear();
                        txtshift.Clear();
                        txtStudName.Clear();
                        txtStudRno.Clear();
                        txtStudAddr.Clear();
                        txtPhonNo.Clear();
                        txtEmail.Clear();
                    }
                    else
                    {
                        //printing_Error_while_executiong_procedure_if_it 
                        MessageBox.Show("Something went wrong");
                    }

                }
                catch (Exception ex)
                {
                    //printing_error_while_executing_try_block
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    con.Close();
                }
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            txtPrnNo.Clear();
            txtGender.Clear();
            txtcountry.Clear();
            txtcource.Clear();
            txtsem.Clear();
            txtshift.Clear();
            txtStudName.Clear();
            txtStudRno.Clear();
            txtStudAddr.Clear();
            txtPhonNo.Clear();
            txtEmail.Clear();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (valid())
            {
                //connection_string
                SqlConnection con = new SqlConnection(@"Data Source=BLACKTECH;Initial Catalog=CollegeManagement;Integrated Security=True");

                try
                {
                    // #Logic : for_fordwording_data_from_txtbox_to_database_by_procedure
                    SqlCommand cmd = new SqlCommand();

                    if (txtPrnNo.Text == "")
                    {
                        cmd.Parameters.AddWithValue("@PRN_NO", null);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@PRN_NO", txtPrnNo.Text);
                    }
                    cmd.Parameters.AddWithValue("@studname", txtStudName.Text);
                    cmd.Parameters.AddWithValue("@studrno", txtStudRno.Text);
                    cmd.Parameters.AddWithValue("@gender", txtGender.Text);
                    cmd.Parameters.AddWithValue("@address", txtStudAddr.Text);
                    cmd.Parameters.AddWithValue("@DOB", date_of_birth.Text.ToString());
                    cmd.Parameters.AddWithValue("@DA", admission_date.Text.ToString());
                    cmd.Parameters.AddWithValue("@phoneno", txtPhonNo.Text);
                    cmd.Parameters.AddWithValue("@emial", txtEmail.Text);
                    cmd.Parameters.AddWithValue("@counrty", txtcountry.Text);
                    cmd.Parameters.AddWithValue("@course", txtcource.Text);
                    cmd.Parameters.AddWithValue("@shift", txtshift.Text);
                    cmd.Parameters.AddWithValue("@semister", txtsem.Text);

                    //name _of_procedure_prescent_in_the_database
                    cmd.CommandText = "p_Student_Admission";
                    cmd.CommandType = CommandType.StoredProcedure;

                    //getting_return_value_when_procedure_is_executed
                    SqlParameter rvalue = cmd.Parameters.Add("@r", SqlDbType.Int);
                    rvalue.Direction = ParameterDirection.ReturnValue;

                    //Execution_of_procedure
                    cmd.Connection = con;
                    con.Open();
                    cmd.ExecuteNonQuery();

                    int r = 0;
                    r = (int)rvalue.Value;

                    //for_success_the_data_enty_into_database
                    if (r > 0)
                    {
                        MessageBox.Show("Data Update Successfully");

                        //#clearing_data_prescent_in_the_txtboxes
                        txtPrnNo.Clear();
                        txtGender.Clear();
                        txtcountry.Clear();
                        txtcource.Clear();
                        txtsem.Clear();
                        txtshift.Clear();
                        txtStudName.Clear();
                        txtStudRno.Clear();
                        txtStudAddr.Clear();
                        txtPhonNo.Clear();
                        txtEmail.Clear();
                    }
                    else
                    {
                        //printing_Error_while_executiong_procedure_if_it 
                        MessageBox.Show("Something went wrong");
                    }

                }
                catch (Exception ex)
                {
                    //printing_error_while_executing_try_block
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    con.Close();
                }
            }
        }

        private void txtPrnNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsNumber(e.KeyChar) & (Keys)e.KeyChar != Keys.Back)
            {
                e.Handled = true;
            }
        }

        private void txtStudName_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) & (Keys)e.KeyChar != Keys.Back)
            {
                e.Handled = true;
            }
        }

        private void txtStudRno_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsNumber(e.KeyChar) & (Keys)e.KeyChar != Keys.Back)
            {
                e.Handled = true;
            }
        }

        private void txtGender_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) & (Keys)e.KeyChar != Keys.Back)
            {
                e.Handled = true;
            }
        }

        private void txtStudAddr_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) & (Keys)e.KeyChar != Keys.Back)
            {
                e.Handled = true;
            }
        }

        private void txtPhonNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsNumber(e.KeyChar) & (Keys)e.KeyChar != Keys.Back)
            {
                e.Handled = true;
            }
        }

        private void txtcountry_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) & (Keys)e.KeyChar != Keys.Back)
            {
                e.Handled = true;
            }
        }

        private void txtcource_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) & (Keys)e.KeyChar != Keys.Back)
            {
                e.Handled = true;
            }
        }

        private void txtshift_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) & (Keys)e.KeyChar != Keys.Back)
            {
                e.Handled = true;
            }
        }

        private void txtsem_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsNumber(e.KeyChar) & (Keys)e.KeyChar != Keys.Back)
            {
                e.Handled = true;
            }
        }
    }
}
