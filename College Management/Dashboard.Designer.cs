﻿namespace College_Management
{
    partial class Dashboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logOutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.teacherToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.assignNewTeacherToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.teacherAttendanceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.attendanceHistoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.scheduleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.scheduleHistoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.studentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newAdmissionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.attandanceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.attToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.feeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.feeHistoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.userToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createAccountToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editProfileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutUsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.teacherToolStripMenuItem,
            this.studentToolStripMenuItem,
            this.userToolStripMenuItem,
            this.aboutUsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 28);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.logOutToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(46, 24);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // logOutToolStripMenuItem
            // 
            this.logOutToolStripMenuItem.Name = "logOutToolStripMenuItem";
            this.logOutToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.logOutToolStripMenuItem.Text = "Log Out";
            this.logOutToolStripMenuItem.Click += new System.EventHandler(this.logOutToolStripMenuItem_Click);
            // 
            // teacherToolStripMenuItem
            // 
            this.teacherToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.assignNewTeacherToolStripMenuItem,
            this.teacherAttendanceToolStripMenuItem,
            this.attendanceHistoryToolStripMenuItem,
            this.scheduleToolStripMenuItem,
            this.scheduleHistoryToolStripMenuItem});
            this.teacherToolStripMenuItem.Name = "teacherToolStripMenuItem";
            this.teacherToolStripMenuItem.Size = new System.Drawing.Size(74, 24);
            this.teacherToolStripMenuItem.Text = "Teacher";
            // 
            // assignNewTeacherToolStripMenuItem
            // 
            this.assignNewTeacherToolStripMenuItem.Name = "assignNewTeacherToolStripMenuItem";
            this.assignNewTeacherToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.assignNewTeacherToolStripMenuItem.Text = "Assign New Teacher";
            this.assignNewTeacherToolStripMenuItem.Click += new System.EventHandler(this.assignNewTeacherToolStripMenuItem_Click);
            // 
            // teacherAttendanceToolStripMenuItem
            // 
            this.teacherAttendanceToolStripMenuItem.Name = "teacherAttendanceToolStripMenuItem";
            this.teacherAttendanceToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.teacherAttendanceToolStripMenuItem.Text = "Teacher Attendance";
            this.teacherAttendanceToolStripMenuItem.Click += new System.EventHandler(this.teacherAttendanceToolStripMenuItem_Click);
            // 
            // attendanceHistoryToolStripMenuItem
            // 
            this.attendanceHistoryToolStripMenuItem.Name = "attendanceHistoryToolStripMenuItem";
            this.attendanceHistoryToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.attendanceHistoryToolStripMenuItem.Text = "Attendance History";
            this.attendanceHistoryToolStripMenuItem.Click += new System.EventHandler(this.attendanceHistoryToolStripMenuItem_Click);
            // 
            // scheduleToolStripMenuItem
            // 
            this.scheduleToolStripMenuItem.Name = "scheduleToolStripMenuItem";
            this.scheduleToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.scheduleToolStripMenuItem.Text = "Schedule";
            this.scheduleToolStripMenuItem.Click += new System.EventHandler(this.scheduleToolStripMenuItem_Click);
            // 
            // scheduleHistoryToolStripMenuItem
            // 
            this.scheduleHistoryToolStripMenuItem.Name = "scheduleHistoryToolStripMenuItem";
            this.scheduleHistoryToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.scheduleHistoryToolStripMenuItem.Text = "Schedule History";
            this.scheduleHistoryToolStripMenuItem.Click += new System.EventHandler(this.scheduleHistoryToolStripMenuItem_Click);
            // 
            // studentToolStripMenuItem
            // 
            this.studentToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newAdmissionToolStripMenuItem,
            this.attandanceToolStripMenuItem,
            this.attToolStripMenuItem,
            this.feeToolStripMenuItem,
            this.feeHistoryToolStripMenuItem});
            this.studentToolStripMenuItem.Name = "studentToolStripMenuItem";
            this.studentToolStripMenuItem.Size = new System.Drawing.Size(74, 24);
            this.studentToolStripMenuItem.Text = "Student";
            // 
            // newAdmissionToolStripMenuItem
            // 
            this.newAdmissionToolStripMenuItem.Name = "newAdmissionToolStripMenuItem";
            this.newAdmissionToolStripMenuItem.Size = new System.Drawing.Size(219, 26);
            this.newAdmissionToolStripMenuItem.Text = "New Admission";
            this.newAdmissionToolStripMenuItem.Click += new System.EventHandler(this.newAdmissionToolStripMenuItem_Click);
            // 
            // attandanceToolStripMenuItem
            // 
            this.attandanceToolStripMenuItem.Name = "attandanceToolStripMenuItem";
            this.attandanceToolStripMenuItem.Size = new System.Drawing.Size(219, 26);
            this.attandanceToolStripMenuItem.Text = "Attendance";
            this.attandanceToolStripMenuItem.Click += new System.EventHandler(this.attandanceToolStripMenuItem_Click);
            // 
            // attToolStripMenuItem
            // 
            this.attToolStripMenuItem.Name = "attToolStripMenuItem";
            this.attToolStripMenuItem.Size = new System.Drawing.Size(219, 26);
            this.attToolStripMenuItem.Text = "Attendance History";
            this.attToolStripMenuItem.Click += new System.EventHandler(this.attToolStripMenuItem_Click);
            // 
            // feeToolStripMenuItem
            // 
            this.feeToolStripMenuItem.Name = "feeToolStripMenuItem";
            this.feeToolStripMenuItem.Size = new System.Drawing.Size(219, 26);
            this.feeToolStripMenuItem.Text = "Fee";
            this.feeToolStripMenuItem.Click += new System.EventHandler(this.feeToolStripMenuItem_Click);
            // 
            // feeHistoryToolStripMenuItem
            // 
            this.feeHistoryToolStripMenuItem.Name = "feeHistoryToolStripMenuItem";
            this.feeHistoryToolStripMenuItem.Size = new System.Drawing.Size(219, 26);
            this.feeHistoryToolStripMenuItem.Text = "Fee History";
            this.feeHistoryToolStripMenuItem.Click += new System.EventHandler(this.feeHistoryToolStripMenuItem_Click);
            // 
            // userToolStripMenuItem
            // 
            this.userToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createAccountToolStripMenuItem,
            this.editProfileToolStripMenuItem});
            this.userToolStripMenuItem.Name = "userToolStripMenuItem";
            this.userToolStripMenuItem.Size = new System.Drawing.Size(56, 24);
            this.userToolStripMenuItem.Text = "User ";
            // 
            // createAccountToolStripMenuItem
            // 
            this.createAccountToolStripMenuItem.Name = "createAccountToolStripMenuItem";
            this.createAccountToolStripMenuItem.Size = new System.Drawing.Size(193, 26);
            this.createAccountToolStripMenuItem.Text = "Create Account";
            this.createAccountToolStripMenuItem.Click += new System.EventHandler(this.createAccountToolStripMenuItem_Click);
            // 
            // editProfileToolStripMenuItem
            // 
            this.editProfileToolStripMenuItem.Name = "editProfileToolStripMenuItem";
            this.editProfileToolStripMenuItem.Size = new System.Drawing.Size(193, 26);
            this.editProfileToolStripMenuItem.Text = "Edit Profile";
            // 
            // aboutUsToolStripMenuItem
            // 
            this.aboutUsToolStripMenuItem.Name = "aboutUsToolStripMenuItem";
            this.aboutUsToolStripMenuItem.Size = new System.Drawing.Size(84, 24);
            this.aboutUsToolStripMenuItem.Text = "About Us";
            // 
            // Dashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Dashboard";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Dashboard";
            this.Load += new System.EventHandler(this.Dashboard_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MenuStrip menuStrip1;
        private ToolStripMenuItem fileToolStripMenuItem;
        private ToolStripMenuItem teacherToolStripMenuItem;
        private ToolStripMenuItem assignNewTeacherToolStripMenuItem;
        private ToolStripMenuItem teacherAttendanceToolStripMenuItem;
        private ToolStripMenuItem attendanceHistoryToolStripMenuItem;
        private ToolStripMenuItem scheduleToolStripMenuItem;
        private ToolStripMenuItem scheduleHistoryToolStripMenuItem;
        private ToolStripMenuItem studentToolStripMenuItem;
        private ToolStripMenuItem newAdmissionToolStripMenuItem;
        private ToolStripMenuItem attandanceToolStripMenuItem;
        private ToolStripMenuItem attToolStripMenuItem;
        private ToolStripMenuItem feeToolStripMenuItem;
        private ToolStripMenuItem feeHistoryToolStripMenuItem;
        private ToolStripMenuItem userToolStripMenuItem;
        private ToolStripMenuItem createAccountToolStripMenuItem;
        private ToolStripMenuItem editProfileToolStripMenuItem;
        private ToolStripMenuItem aboutUsToolStripMenuItem;
        private ToolStripMenuItem logOutToolStripMenuItem;
    }
}