﻿namespace College_Management
{
    partial class AttendenceHistory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnSearchPRN = new System.Windows.Forms.Button();
            this.txtprnSearch = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnSearchPRN);
            this.groupBox1.Controls.Add(this.txtprnSearch);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 24);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(766, 72);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "[ Search Attendance ]";
            // 
            // btnSearchPRN
            // 
            this.btnSearchPRN.Location = new System.Drawing.Point(606, 33);
            this.btnSearchPRN.Name = "btnSearchPRN";
            this.btnSearchPRN.Size = new System.Drawing.Size(94, 29);
            this.btnSearchPRN.TabIndex = 2;
            this.btnSearchPRN.Text = "&Search";
            this.btnSearchPRN.UseVisualStyleBackColor = true;
            // 
            // txtprnSearch
            // 
            this.txtprnSearch.Location = new System.Drawing.Point(95, 27);
            this.txtprnSearch.Name = "txtprnSearch";
            this.txtprnSearch.Size = new System.Drawing.Size(484, 27);
            this.txtprnSearch.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "PRN No.";
            // 
            // AttendenceHistory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.groupBox1);
            this.Name = "AttendenceHistory";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Attendence History";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private GroupBox groupBox1;
        private Button btnSearchPRN;
        private TextBox txtprnSearch;
        private Label label1;
    }
}