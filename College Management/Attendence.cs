﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace College_Management
{
    public partial class Attendence : Form
    {
        public Attendence()
        {
            InitializeComponent();
        }

        private Boolean valid()
        { 
            if(txtCource.Text == "")
            {
                MessageBox.Show("Enter Cource");
                txtCource.Focus();
                return false;
            }
            if(txtRollNo.Text == "")
            {
                MessageBox.Show("Enter ROll Number");
                txtRollNo.Focus();
                return false;
            }
            if(txtSemister.Text == "")
            {
                MessageBox.Show("Enter Semister");
                txtSemister.Focus();
                return false;
            }
            if(txtStudentName.Text == "")
            {
                MessageBox.Show("Enter Name");
                txtStudentName.Focus();
                return false;
            }
            return true;
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            txtCource.Clear();
            txtRollNo.Clear();
            txtSemister.Clear();
            txtStudentName.Clear();
            txtSearchRecrd.Clear();
            txtAttendance.Clear();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if(valid())
            {
                //connection_string
                SqlConnection con = new SqlConnection(@"Data Source=BLACKTECH;Initial Catalog=CollegeManagement;Integrated Security=True");

                try
                {
                    // #Logic : for_fordwording_data_from_txtbox_to_database_by_procedure
                    SqlCommand cmd = new SqlCommand();

                    if (txtRollNo.Text == "")
                    {
                        cmd.Parameters.AddWithValue("@Rno", null);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@Rno", txtRollNo.Text);
                    }
                    cmd.Parameters.AddWithValue("@cource", txtCource.Text);
                    cmd.Parameters.AddWithValue("@semister", txtSemister.Text);
                    cmd.Parameters.AddWithValue("@studentname", txtStudentName.Text);
                    cmd.Parameters.AddWithValue("@Adt", attendance_date.Text.ToString());
                    cmd.Parameters.AddWithValue("@attendance", txtAttendance.Text);

                    //name _of_procedure_prescent_in_the_database
                    cmd.CommandText = "p_Attendance";
                    cmd.CommandType = CommandType.StoredProcedure;

                    //getting_return_value_when_procedure_is_executed
                    SqlParameter rvalue = cmd.Parameters.Add("@r", SqlDbType.Int);
                    rvalue.Direction = ParameterDirection.ReturnValue;

                    //Execution_of_procedure
                    cmd.Connection = con;
                    con.Open();
                    cmd.ExecuteNonQuery();

                    int r = 0;
                    r = (int)rvalue.Value;

                    //for_success_the_data_enty_into_database
                    if (r > 0)
                    {
                        MessageBox.Show("Data Saved Successfully");

                        //#clearing_data_prescent_in_the_txtboxes
                        txtCource.Clear();
                        txtRollNo.Clear();
                        txtSemister.Clear();
                        txtStudentName.Clear();
                        txtSearchRecrd.Clear();
                        txtAttendance.Clear();
                    }
                    else
                    {
                        //printing_Error_while_executiong_procedure_if_it 
                        MessageBox.Show("Something went wrong");
                    }
                }
                catch (Exception ex)
                {
                    //printing_error_while_executing_try_block
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    con.Close();
                }
            }
        }

        private void txtCource_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) & (Keys)e.KeyChar != Keys.Back)
            {
                e.Handled = true;
            }
        }

        private void txtSemister_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) & (Keys)e.KeyChar != Keys.Back)
            {
                e.Handled = true;
            }
        }

        private void txtStudentName_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) & (Keys)e.KeyChar != Keys.Back)
            {
                e.Handled = true;
            }
        }

        private void txtRollNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) & (Keys)e.KeyChar != Keys.Back)
            {
                e.Handled = true;
            }
        }

        private void txtAttendance_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) & (Keys)e.KeyChar != Keys.Back)
            {
                e.Handled = true;
            }
        }
    }
}
