﻿namespace College_Management
{
    partial class StudentAdmission
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GSearchRecord = new System.Windows.Forms.GroupBox();
            this.btnSearchRecord = new System.Windows.Forms.Button();
            this.txtSearchRecrd = new System.Windows.Forms.TextBox();
            this.GAddStudent = new System.Windows.Forms.GroupBox();
            this.txtshift = new System.Windows.Forms.TextBox();
            this.txtsem = new System.Windows.Forms.TextBox();
            this.txtcource = new System.Windows.Forms.TextBox();
            this.txtcountry = new System.Windows.Forms.TextBox();
            this.txtGender = new System.Windows.Forms.TextBox();
            this.txtPrnNo = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.txtPhonNo = new System.Windows.Forms.TextBox();
            this.txtStudName = new System.Windows.Forms.TextBox();
            this.txtStudRno = new System.Windows.Forms.TextBox();
            this.txtStudAddr = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.btnStudPicUpload = new System.Windows.Forms.Button();
            this.pboxstudentupload = new System.Windows.Forms.PictureBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.date_of_birth = new System.Windows.Forms.DateTimePicker();
            this.admission_date = new System.Windows.Forms.DateTimePicker();
            this.GSearchRecord.SuspendLayout();
            this.GAddStudent.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pboxstudentupload)).BeginInit();
            this.SuspendLayout();
            // 
            // GSearchRecord
            // 
            this.GSearchRecord.Controls.Add(this.btnSearchRecord);
            this.GSearchRecord.Controls.Add(this.txtSearchRecrd);
            this.GSearchRecord.Location = new System.Drawing.Point(16, 16);
            this.GSearchRecord.Name = "GSearchRecord";
            this.GSearchRecord.Size = new System.Drawing.Size(772, 70);
            this.GSearchRecord.TabIndex = 0;
            this.GSearchRecord.TabStop = false;
            this.GSearchRecord.Text = "[ Search Record ]";
            // 
            // btnSearchRecord
            // 
            this.btnSearchRecord.Location = new System.Drawing.Point(608, 30);
            this.btnSearchRecord.Name = "btnSearchRecord";
            this.btnSearchRecord.Size = new System.Drawing.Size(94, 29);
            this.btnSearchRecord.TabIndex = 1;
            this.btnSearchRecord.Text = "&Search";
            this.btnSearchRecord.UseVisualStyleBackColor = true;
            // 
            // txtSearchRecrd
            // 
            this.txtSearchRecrd.Location = new System.Drawing.Point(57, 26);
            this.txtSearchRecrd.Name = "txtSearchRecrd";
            this.txtSearchRecrd.Size = new System.Drawing.Size(518, 27);
            this.txtSearchRecrd.TabIndex = 0;
            // 
            // GAddStudent
            // 
            this.GAddStudent.Controls.Add(this.admission_date);
            this.GAddStudent.Controls.Add(this.date_of_birth);
            this.GAddStudent.Controls.Add(this.txtshift);
            this.GAddStudent.Controls.Add(this.txtsem);
            this.GAddStudent.Controls.Add(this.txtcource);
            this.GAddStudent.Controls.Add(this.txtcountry);
            this.GAddStudent.Controls.Add(this.txtGender);
            this.GAddStudent.Controls.Add(this.txtPrnNo);
            this.GAddStudent.Controls.Add(this.label14);
            this.GAddStudent.Controls.Add(this.label13);
            this.GAddStudent.Controls.Add(this.btnClose);
            this.GAddStudent.Controls.Add(this.btnDelete);
            this.GAddStudent.Controls.Add(this.btnUpdate);
            this.GAddStudent.Controls.Add(this.btnSave);
            this.GAddStudent.Controls.Add(this.txtEmail);
            this.GAddStudent.Controls.Add(this.txtPhonNo);
            this.GAddStudent.Controls.Add(this.txtStudName);
            this.GAddStudent.Controls.Add(this.txtStudRno);
            this.GAddStudent.Controls.Add(this.txtStudAddr);
            this.GAddStudent.Controls.Add(this.label12);
            this.GAddStudent.Controls.Add(this.label11);
            this.GAddStudent.Controls.Add(this.btnStudPicUpload);
            this.GAddStudent.Controls.Add(this.pboxstudentupload);
            this.GAddStudent.Controls.Add(this.label10);
            this.GAddStudent.Controls.Add(this.label9);
            this.GAddStudent.Controls.Add(this.label8);
            this.GAddStudent.Controls.Add(this.label7);
            this.GAddStudent.Controls.Add(this.label6);
            this.GAddStudent.Controls.Add(this.label5);
            this.GAddStudent.Controls.Add(this.label4);
            this.GAddStudent.Controls.Add(this.label3);
            this.GAddStudent.Controls.Add(this.label2);
            this.GAddStudent.Controls.Add(this.label1);
            this.GAddStudent.Location = new System.Drawing.Point(22, 109);
            this.GAddStudent.Name = "GAddStudent";
            this.GAddStudent.Size = new System.Drawing.Size(935, 475);
            this.GAddStudent.TabIndex = 1;
            this.GAddStudent.TabStop = false;
            this.GAddStudent.Text = "[ Student Admission Details ]";
            // 
            // txtshift
            // 
            this.txtshift.Location = new System.Drawing.Point(647, 297);
            this.txtshift.Name = "txtshift";
            this.txtshift.Size = new System.Drawing.Size(254, 27);
            this.txtshift.TabIndex = 41;
            this.txtshift.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtshift_KeyPress);
            // 
            // txtsem
            // 
            this.txtsem.Location = new System.Drawing.Point(647, 339);
            this.txtsem.Name = "txtsem";
            this.txtsem.Size = new System.Drawing.Size(254, 27);
            this.txtsem.TabIndex = 40;
            this.txtsem.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtsem_KeyPress);
            // 
            // txtcource
            // 
            this.txtcource.Location = new System.Drawing.Point(133, 372);
            this.txtcource.Name = "txtcource";
            this.txtcource.Size = new System.Drawing.Size(385, 27);
            this.txtcource.TabIndex = 39;
            this.txtcource.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtcource_KeyPress);
            // 
            // txtcountry
            // 
            this.txtcountry.Location = new System.Drawing.Point(134, 339);
            this.txtcountry.Name = "txtcountry";
            this.txtcountry.Size = new System.Drawing.Size(385, 27);
            this.txtcountry.TabIndex = 38;
            this.txtcountry.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtcountry_KeyPress);
            // 
            // txtGender
            // 
            this.txtGender.Location = new System.Drawing.Point(134, 126);
            this.txtGender.Name = "txtGender";
            this.txtGender.Size = new System.Drawing.Size(385, 27);
            this.txtGender.TabIndex = 37;
            this.txtGender.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtGender_KeyPress);
            // 
            // txtPrnNo
            // 
            this.txtPrnNo.Location = new System.Drawing.Point(134, 25);
            this.txtPrnNo.Name = "txtPrnNo";
            this.txtPrnNo.Size = new System.Drawing.Size(385, 27);
            this.txtPrnNo.TabIndex = 36;
            this.txtPrnNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPrnNo_KeyPress);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(14, 32);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(64, 20);
            this.label14.TabIndex = 35;
            this.label14.Text = "PRN No.";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(715, 37);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(91, 20);
            this.label13.TabIndex = 34;
            this.label13.Text = "Insert Image";
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(558, 422);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(94, 29);
            this.btnClose.TabIndex = 33;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(439, 422);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(94, 29);
            this.btnDelete.TabIndex = 32;
            this.btnDelete.Text = "&Clear";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(320, 422);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(94, 29);
            this.btnUpdate.TabIndex = 31;
            this.btnUpdate.Text = "&Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(201, 422);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(94, 29);
            this.btnSave.TabIndex = 30;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(133, 302);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(385, 27);
            this.txtEmail.TabIndex = 21;
            // 
            // txtPhonNo
            // 
            this.txtPhonNo.Location = new System.Drawing.Point(133, 265);
            this.txtPhonNo.Name = "txtPhonNo";
            this.txtPhonNo.Size = new System.Drawing.Size(385, 27);
            this.txtPhonNo.TabIndex = 20;
            this.txtPhonNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPhonNo_KeyPress);
            // 
            // txtStudName
            // 
            this.txtStudName.Location = new System.Drawing.Point(133, 58);
            this.txtStudName.Name = "txtStudName";
            this.txtStudName.Size = new System.Drawing.Size(385, 27);
            this.txtStudName.TabIndex = 19;
            this.txtStudName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtStudName_KeyPress);
            // 
            // txtStudRno
            // 
            this.txtStudRno.Location = new System.Drawing.Point(133, 94);
            this.txtStudRno.Name = "txtStudRno";
            this.txtStudRno.Size = new System.Drawing.Size(385, 27);
            this.txtStudRno.TabIndex = 18;
            this.txtStudRno.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtStudRno_KeyPress);
            // 
            // txtStudAddr
            // 
            this.txtStudAddr.Location = new System.Drawing.Point(133, 159);
            this.txtStudAddr.Name = "txtStudAddr";
            this.txtStudAddr.Size = new System.Drawing.Size(385, 27);
            this.txtStudAddr.TabIndex = 17;
            this.txtStudAddr.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtStudAddr_KeyPress);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(571, 342);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(70, 20);
            this.label12.TabIndex = 15;
            this.label12.Text = "Semester";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(571, 304);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(39, 20);
            this.label11.TabIndex = 14;
            this.label11.Text = "Shift";
            // 
            // btnStudPicUpload
            // 
            this.btnStudPicUpload.Location = new System.Drawing.Point(717, 253);
            this.btnStudPicUpload.Name = "btnStudPicUpload";
            this.btnStudPicUpload.Size = new System.Drawing.Size(94, 29);
            this.btnStudPicUpload.TabIndex = 13;
            this.btnStudPicUpload.Text = "&Upload";
            this.btnStudPicUpload.UseVisualStyleBackColor = true;
            // 
            // pboxstudentupload
            // 
            this.pboxstudentupload.Location = new System.Drawing.Point(672, 68);
            this.pboxstudentupload.Name = "pboxstudentupload";
            this.pboxstudentupload.Size = new System.Drawing.Size(181, 179);
            this.pboxstudentupload.TabIndex = 12;
            this.pboxstudentupload.TabStop = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(13, 379);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(54, 20);
            this.label10.TabIndex = 11;
            this.label10.Text = "Course";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(13, 342);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(60, 20);
            this.label9.TabIndex = 10;
            this.label9.Text = "Country";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(13, 305);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(46, 20);
            this.label8.TabIndex = 9;
            this.label8.Text = "Email";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(13, 268);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(77, 20);
            this.label7.TabIndex = 8;
            this.label7.Text = "Phone No.";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(13, 231);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(114, 20);
            this.label6.TabIndex = 7;
            this.label6.Text = "Admission Date";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 194);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(96, 20);
            this.label5.TabIndex = 6;
            this.label5.Text = "Date Of Birth";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 162);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(62, 20);
            this.label4.TabIndex = 5;
            this.label4.Text = "Address";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(14, 129);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "Gender";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 94);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Roll No.";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 61);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Student Name";
            // 
            // date_of_birth
            // 
            this.date_of_birth.Location = new System.Drawing.Point(133, 195);
            this.date_of_birth.Name = "date_of_birth";
            this.date_of_birth.Size = new System.Drawing.Size(264, 27);
            this.date_of_birth.TabIndex = 42;
            // 
            // admission_date
            // 
            this.admission_date.Location = new System.Drawing.Point(134, 232);
            this.admission_date.Name = "admission_date";
            this.admission_date.Size = new System.Drawing.Size(263, 27);
            this.admission_date.TabIndex = 43;
            // 
            // StudentAdmission
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(991, 609);
            this.Controls.Add(this.GAddStudent);
            this.Controls.Add(this.GSearchRecord);
            this.Name = "StudentAdmission";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "StudentAdmission";
            this.Load += new System.EventHandler(this.StudentAdmission_Load);
            this.GSearchRecord.ResumeLayout(false);
            this.GSearchRecord.PerformLayout();
            this.GAddStudent.ResumeLayout(false);
            this.GAddStudent.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pboxstudentupload)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private GroupBox GSearchRecord;
        private Button btnSearchRecord;
        private TextBox txtSearchRecrd;
        private GroupBox GAddStudent;
        private TextBox txtEmail;
        private TextBox txtPhonNo;
        private TextBox txtStudName;
        private TextBox txtStudRno;
        private TextBox txtStudAddr;
        private Label label12;
        private Label label11;
        private Button btnStudPicUpload;
        private PictureBox pboxstudentupload;
        private Label label10;
        private Label label9;
        private Label label8;
        private Label label7;
        private Label label6;
        private Label label5;
        private Label label4;
        private Label label3;
        private Label label2;
        private Label label1;
        private Button btnClose;
        private Button btnDelete;
        private Button btnUpdate;
        private Button btnSave;
        private Label label13;
        private TextBox txtPrnNo;
        private Label label14;
        private TextBox txtshift;
        private TextBox txtsem;
        private TextBox txtcource;
        private TextBox txtcountry;
        private TextBox txtGender;
        private DateTimePicker admission_date;
        private DateTimePicker date_of_birth;
    }
}