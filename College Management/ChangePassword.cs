﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace College_Management
{
    public partial class ChangePassword : Form
    {
        public ChangePassword()
        {
            InitializeComponent();
        }

        private Boolean valid()
        {
            if(txtUsername.Text == "")
            {
                MessageBox.Show("Enter Username");
                txtUsername.Focus();
                return false;
            }
            if(txtPassword.Text == "")
            {
                MessageBox.Show("Enter Password");
                txtPassword.Focus();
                return false;
            }
            if(txtConPassword.Text == "")
            {
                MessageBox.Show("Enter Confirm Password");
                txtConPassword.Focus();
                return false;
            }
            return true;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtUsername_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) & (Keys)e.KeyChar != Keys.Back)
            {
                e.Handled = true;
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if(valid())
            {
                //connection_string
                SqlConnection con = new SqlConnection(@"Data Source=BLACKTECH;Initial Catalog=CollegeManagement;Integrated Security=True");

                try
                {
                    // #Logic : for_fordwording_data_from_txtbox_to_database_by_procedure
                    SqlCommand cmd = new SqlCommand();

                    cmd.Parameters.AddWithValue("@username", txtUsername.Text);
                    cmd.Parameters.AddWithValue("@password", txtPassword.Text);
                    cmd.Parameters.AddWithValue("@conpassword", txtConPassword.Text);

                    //name _of_procedure_prescent_in_the_database
                    cmd.CommandText = "p_createuser";
                    cmd.CommandType = CommandType.StoredProcedure;

                    //getting_return_value_when_procedure_is_executed
                    SqlParameter rvalue = cmd.Parameters.Add("@r", SqlDbType.Int);
                    rvalue.Direction = ParameterDirection.ReturnValue;

                    //Execution_of_procedure
                    cmd.Connection = con;
                    con.Open();
                    cmd.ExecuteNonQuery();

                    int r = 0;
                    r = (int)rvalue.Value;

                    //for_success_the_data_enty_into_database
                    if (r > 0)
                    {
                        MessageBox.Show("Data Saved Successfully");

                        //#clearing_data_prescent_in_the_txtboxes
                        txtConPassword.Clear();
                        txtPassword.Clear();
                        txtUsername.Clear();
                    }
                    else
                    {
                        //printing_Error_while_executiong_procedure_if_it 
                        MessageBox.Show("Something went wrong");
                    }
                }
                catch (Exception ex)
                {
                    //printing_error_while_executing_try_block
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    con.Close();
                }
            }
        }
    }
}
