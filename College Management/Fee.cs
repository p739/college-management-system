﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace College_Management
{
    public partial class Fee : Form
    {
        public Fee()
        {
            InitializeComponent();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            txtStudName.Clear();
            txtSearchPRN.Clear();
            txtCource.Clear();
            txtCatagory.Clear();
            txtDueAmount.Clear();
            txtPaidAmount.Clear();
            txtRollNo.Clear();
            txtSemister.Clear();
            txtAmount.Clear();
        }

        private void Fee_Load(object sender, EventArgs e)
        {

        }

        private Boolean valid()
        {
            if(txtStudName.Text=="")
            {
                MessageBox.Show("Please Enter Student Name");
                txtStudName.Focus();
                return false;
            }
            if(txtRollNo.Text=="")
            {
                MessageBox.Show("Please Enter Student Roll No.");
                txtRollNo.Focus();
                return false;
            }
            if(txtCource.Text=="")
            {
                MessageBox.Show("Please Enter Cource");
                txtCource.Focus();
                return false;
            }
            if(txtSemister.Text=="")
            {
                MessageBox.Show("Please Enter Student Semister");
                txtSemister.Focus();
                return false;
            }
            if(txtCatagory.Text=="")
            {
                MessageBox.Show("Please Enter Catagory");
                txtCatagory.Focus();
                return false;
            }
            if(txtAmount.Text=="")
            {
                MessageBox.Show("Please Enter Amount");
                txtAmount.Focus();
                return false;
            }
            if(txtPaidAmount.Text=="")
            {
                MessageBox.Show("Please Enter Paid Amount");
                txtPaidAmount.Focus();
                return false;
            }
            if(txtDueAmount.Text=="")
            {
                MessageBox.Show("Please Enter Due Amount");
                txtDueAmount.Focus();
                return false;
            }
            return true;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if(valid())
            {
                //connection_string
                SqlConnection con = new SqlConnection(@"Data Source=BLACKTECH;Initial Catalog=CollegeManagement;Integrated Security=True");
                try
                { 
                    // #Logic : for_fordwording_data_from_txtbox_to_database_by_procedure
                    SqlCommand cmd = new SqlCommand();

                    if(txtRollNo.Text=="")
                    {
                        cmd.Parameters.AddWithValue("@rno",null);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@rno", txtRollNo.Text);
                    }

                    cmd.Parameters.AddWithValue("@studname", txtStudName.Text);
                    cmd.Parameters.AddWithValue("@course", txtCource.Text);
                    cmd.Parameters.AddWithValue("@semister", txtSemister.Text);
                    cmd.Parameters.AddWithValue("@catagory", txtCatagory.Text);
                    cmd.Parameters.AddWithValue("@amount", txtAmount.Text);
                    cmd.Parameters.AddWithValue("@paidamount", txtPaidAmount.Text);
                    cmd.Parameters.AddWithValue("@dueamount",txtDueAmount.Text);
                    cmd.Parameters.AddWithValue("@paiddate", Paid_Date_fee.Text.ToString());

                    //name _of_procedure_prescent_in_the_database
                    cmd.CommandText = "p_fee";
                    cmd.CommandType = CommandType.StoredProcedure;

                    //getting_return_value_when_procedure_is_executed
                    SqlParameter rvalue = cmd.Parameters.Add("@r", SqlDbType.Int);
                    rvalue.Direction = ParameterDirection.ReturnValue;

                    //Execution_of_procedure
                    cmd.Connection = con;
                    con.Open();
                    cmd.ExecuteNonQuery();

                    int r = 0;
                    r = (int)rvalue.Value;

                    //for_success_the_data_enty_into_database
                    if (r > 0)
                    {
                        MessageBox.Show("Fee Added Successfully");

                        //#clearing_data_prescent_in_the_txtboxes
                        txtStudName.Clear();
                        txtSearchPRN.Clear();
                        txtCource.Clear();
                        txtCatagory.Clear();
                        txtDueAmount.Clear();
                        txtPaidAmount.Clear();
                        txtRollNo.Clear();
                        txtSemister.Clear();
                        txtAmount.Clear();
                    }
                    else
                    {
                        //printing_Error_while_executiong_procedure_if_it 
                        MessageBox.Show("Something went wrong");
                    }
                }
                catch (Exception ex)
                {
                    //printing_error_while_executing_try_block
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    con.Close();
                }
            }
        }

        private void btnFeeUpdate_Click(object sender, EventArgs e)
        {
            if(valid())
            {
                //connection_string
                SqlConnection con = new SqlConnection(@"Data Source=BLACKTECH;Initial Catalog=CollegeManagement;Integrated Security=True");

                try
                {
                    // #Logic : for_fordwording_data_from_txtbox_to_database_by_procedure
                    SqlCommand cmd = new SqlCommand();

                    if (txtRollNo.Text == "")
                    {
                        cmd.Parameters.AddWithValue("@rno", null);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@rno", txtRollNo.Text);
                    }

                    cmd.Parameters.AddWithValue("@studname", txtStudName.Text);
                    cmd.Parameters.AddWithValue("@course", txtCource.Text);
                    cmd.Parameters.AddWithValue("@semister", txtSemister.Text);
                    cmd.Parameters.AddWithValue("@catagory", txtCatagory.Text);
                    cmd.Parameters.AddWithValue("@amount", txtAmount.Text);
                    cmd.Parameters.AddWithValue("@paidamount", txtPaidAmount.Text);
                    cmd.Parameters.AddWithValue("@dueamount", txtDueAmount.Text);
                    cmd.Parameters.AddWithValue("@paiddate", Paid_Date_fee.Text.ToString());

                    //name _of_procedure_prescent_in_the_database
                    cmd.CommandText = "p_fee";
                    cmd.CommandType = CommandType.StoredProcedure;

                    //getting_return_value_when_procedure_is_executed
                    SqlParameter rvalue = cmd.Parameters.Add("@r", SqlDbType.Int);
                    rvalue.Direction = ParameterDirection.ReturnValue;

                    //Execution_of_procedure
                    cmd.Connection = con;
                    con.Open();
                    cmd.ExecuteNonQuery();

                    int r = 0;
                    r = (int)rvalue.Value;

                    //for_success_the_data_enty_into_database
                    if (r > 0)
                    {
                        MessageBox.Show("Fee Updated Successfully");

                        //#clearing_data_prescent_in_the_txtboxes
                        txtStudName.Clear();
                        txtSearchPRN.Clear();
                        txtCource.Clear();
                        txtCatagory.Clear();
                        txtDueAmount.Clear();
                        txtPaidAmount.Clear();
                        txtRollNo.Clear();
                        txtSemister.Clear();
                        txtAmount.Clear();
                    }
                    else
                    {
                        //printing_Error_while_executiong_procedure_if_it 
                        MessageBox.Show("Something went wrong");
                    }
                }
                catch (Exception ex)
                {
                    //printing_error_while_executing_try_block
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    con.Close();
                }
            }
        }

        private void txtStudName_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(!char.IsLetter(e.KeyChar) & (Keys)e.KeyChar != Keys.Back)
            {
                e.Handled= true;
            }
        }

        private void txtPaidAmount_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(!char.IsNumber(e.KeyChar) & (Keys)e.KeyChar != Keys.Back)
            {
                e.Handled = true;
            }
        }

        private void txtRollNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsNumber(e.KeyChar) & (Keys)e.KeyChar != Keys.Back)
            {
                e.Handled = true;
            }
        }

        private void txtAmount_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsNumber(e.KeyChar) & (Keys)e.KeyChar != Keys.Back)
            {
                e.Handled = true;
            }
        }

        private void txtDueAmount_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsNumber(e.KeyChar) & (Keys)e.KeyChar != Keys.Back)
            {
                e.Handled = true;
            }
        }

        private void txtSemister_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsNumber(e.KeyChar) & (Keys)e.KeyChar != Keys.Back)
            {
                e.Handled = true;
            }
        }

        private void txtCource_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) & (Keys)e.KeyChar != Keys.Back)
            {
                e.Handled = true;
            }
        }

        private void txtCatagory_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) & (Keys)e.KeyChar != Keys.Back)
            {
                e.Handled = true;
            }
        }
    }
}
