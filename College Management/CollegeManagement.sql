-- Create_DataBAse
CREATE DATABASE CollegeManagement;

-- Use_DataBase
USE CollegeManagement;

-- Create_table_for_Student_admission
CREATE TABLE tbl_student_admission(
PRN_NO INT CONSTRAINT[PK_tbl_student_admission] PRIMARY KEY IDENTITY,
studname VARCHAR(50) CONSTRAINT[UK_tbl_student_admission] UNIQUE,
studrno INT,
gender VARCHAR(10),
address VARCHAR(50),
DOB INT,
DA INT,
phoneno INT,
email VARCHAR(30),
counrty VARCHAR(10),
course VARCHAR(10),
shift VARCHAR(10),
semister VARCHAR(10),
);

-- Creatating_procedure_for_Student_admission
CREATE PROCEDURE p_Student_Admission
@PRN_NO INT = NULL,
@studname VARCHAR(20) = NULL,
@studrno INT = NULL,
@gender VARCHAR(20) = NULL,
@address VARCHAR(20) = NULL,
@DOB INT = NULL,
@DA INT = NULL,
@phoneno INT = NULL,
@emial VARCHAR(20) = NULL,
@counrty VARCHAR(20) = NULL,
@course VARCHAR(20) = NULL,
@shift VARCHAR(20) = NULL,
@semister VARCHAR(20) = NULL OUTPUT AS
BEGIN TRAN
	BEGIN TRY
	if @PRN_NO IS NULL
	BEGIN 
	SET @PRN_NO = @@IDENTITY
	INSERT INTO tbl_student_admission(PRN_NO, studname, studrno, gender, address, DOB, DA, phoneno, email, counrty, course, shift, semister)
	VALUES (@PRN_NO, @studname, @studrno, @gender, @address, @DOB, @DA, @phoneno, @emial, @counrty, @course, @shift, @semister)
	END
	ELSE
	BEGIN
		UPDATE tbl_student_admission SET studname=@studname, studrno=@studrno, gender=@gender, address=@address, DOB=@DOB, DA=@DA, phoneno=@phoneno, email=@emial, counrty=@counrty, course=@course, shift=@shift, semister=@semister
		WHERE PRN_NO=@PRN_NO
	END
	END TRY
	BEGIN CATCH
	ROLLBACK TRAN
		RETURN -1
	END CATCH
	COMMIT TRAN
		RETURN 1

-- View_tables
SELECT * FROM tbl_student_admission;

-- #create_table_to_add_fee
CREATE TABLE fee(
studname VARCHAR(20),
rno INT,
course VARCHAR(20),
semister VARCHAR(20),
catagory VARCHAR(20),
amount INT,
paidamount INT,
dueamount INT,
paiddate VARCHAR(50)
);

-- #create_procedure_for_fee
CREATE PROCEDURE p_fee
@studname VARCHAR(20) = NULL,
@rno INT = NULL,
@course VARCHAR(20) = NULL,
@semister VARCHAR(20) = NULL,
@catagory VARCHAR(20) = NULL,
@amount INT = NULL,
@paidamount INT = NULL,
@dueamount INT = NULL,
@paiddate VARCHAR(50) OUTPUT AS
BEGIN TRAN
	BEGIN TRY
	if @rno IS NULL
	BEGIN 
	SET @rno = @@IDENTITY
	INSERT INTO fee(studname, rno, course, semister, catagory, amount, paidamount, dueamount, paiddate)
	VALUES (@studname, @rno, @course, @semister, @catagory, @amount, @paidamount, @dueamount, @paiddate)
	END
	ELSE
	BEGIN
		UPDATE fee SET studname=@studname, course=@course, semister=@semister, catagory=@catagory, amount=@amount, paidamount=@paidamount, dueamount=@dueamount, paiddate=@paiddate
		WHERE rno=@rno
	END
	END TRY
	BEGIN CATCH
	ROLLBACK TRAN
		RETURN -1
	END CATCH
	COMMIT TRAN
		RETURN 1

-- #Cretae table for attendance
CREATE TABLE attendance (
rno INT CONSTRAINT[PK_attendance] PRIMARY KEY IDENTITY,
studentname VARCHAR(50) CONSTRAINT[UK_attendace] UNIQUE,
cource VARCHAR(10),
semister VARCHAR(10),
Adt INT,
attendance VARCHAR(10));


-- #Create procedure for attendace table
CREATE PROCEDURE p_Attendance
@Rno INT = NULL,
@studentname VARCHAR(10) = NULL,
@cource VARCHAR(10) = NULL,
@semister VARCHAR(10) = NULL,
@Adt INT = NULL,
@attendance VARCHAR(10) = NULL OUTPUT AS
BEGIN TRAN
	BEGIN TRY
	if @Rno IS NULL
	BEGIN 
	SET @Rno = @@IDENTITY
	INSERT INTO attendance (Rno, studentname, cource, semister, Adt, attendance)
	VALUES (@Rno, @studentname, @cource, @semister, @Adt, @attendance)
	END
	END TRY
	BEGIN CATCH
	ROLLBACK TRAN
		RETURN -1
	END CATCH
	COMMIT TRAN
		RETURN 1

-- #Create table for assign teacher
CREATE TABLE assignteacher (
regno INT CONSTRAINT[PK_assignteacher] PRIMARY KEY IDENTITY,
teachername VARCHAR(50) CONSTRAINT[UK_assignteacher] UNIQUE,
teacheraddr VARCHAR(50),
gender VARCHAR(10),
email VARCHAR(50),
experience VARCHAR(20),
phoneno INT,
speci VARCHAR(50),
shift VARCHAR(20),
dob INT,
ad INT);

-- #create procedure for assign teacher
CREATE PROCEDURE p_Assign_Teacher
@regno INT = NULL,
@teachername VARCHAR(50) = NULL,
@teacheraddr VARCHAR(50) = NULL,
@gender VARCHAR(10) = NULL,
@email VARCHAR(50) = NULL,
@experience VARCHAR(20) = NULL,
@phoneno INT = NULL,
@speci VARCHAR(50) = NULL,
@shift VARCHAR(20) = NULL,
@dob INT = NULL,
@ad INT OUTPUT AS
BEGIN TRAN
	BEGIN TRY
	if @regno IS NULL
	BEGIN 
	SET @regno = @@IDENTITY
		INSERT INTO assignteacher (regno, teachername, teacheraddr, gender, email, experience, phoneno, speci, shift, dob, ad )
		VALUES (@regno, @teachername, @teacheraddr, @gender, @email, @experience, @phoneno, @speci, @shift, @dob, @ad)
	END
	ELSE
	BEGIN
		UPDATE assignteacher SET teachername=@teachername, teacheraddr=@teacheraddr, gender=@gender, email=@email, experience=@experience, phoneno=@phoneno, speci=@speci, dob=@dob, ad=@ad
		WHERE regno=@regno
	END
	END TRY
	BEGIN CATCH
	ROLLBACK TRAN
		RETURN -1
	END CATCH
	COMMIT TRAN
		RETURN 1


-- #create table for teacher attendance
CREATE TABLE teacherattendance(
teachername VARCHAR(50),
attendancedate INT,
attendance VARCHAR(10) );

-- #create procedure for teacher attendanc
CREATE PROCEDURE p_Teacher_Attendance
@teachername VARCHAR(50) = NULL,
@attendance VARCHAR(10) = NULL,
@attendancedate INT OUTPUT AS
BEGIN TRAN
	BEGIN TRY
	if @teachername IS NULL
	BEGIN 
	SET @teachername = @@IDENTITY
	INSERT INTO teacherattendance (teachername, @attendance, @attendancedate)
	VALUES (@teachername, @attendance, @attendancedate)
	END
	END TRY
	BEGIN CATCH
	ROLLBACK TRAN
		RETURN -1
	END CATCH
	COMMIT TRAN
		RETURN 1

-- #create table for teacher period schedule
CREATE TABLE teacherscedule(
teachername VARCHAR(50),
cource VARCHAR(20),
semister VARCHAR(20),
subname VARCHAR(20),
day VARCHAR(10),
shift VARCHAR(10),
time VARCHAR(20),
period VARCHAR(10) );

-- #Create procedure for teacherschedule
CREATE PROCEDURE p_Teacher_Schedule
@teachername VARCHAR(50) = NULL,
@cource VARCHAR(20) = NULL,
@semister VARCHAR(20) = NULL,
@subname VARCHAR(20) = NULL,
@day VARCHAR(10) = NULL,
@shift VARCHAR(10) = NULL,
@time VARCHAR(20) = NULL,
@period VARCHAR(10) OUTPUT AS
BEGIN TRAN
	BEGIN TRY
	if @teachername IS NULL
	BEGIN 
	SET @teachername = @@IDENTITY
	INSERT INTO teacherscedule (teachername, cource, semister, subname, day, shift, time, period)
	VALUES (@teachername, @cource, @semister, @subname, @day, @shift, @time, @period)
	END
	END TRY
	BEGIN CATCH
	ROLLBACK TRAN
		RETURN -1
	END CATCH
	COMMIT TRAN

-- #create table for new user
CREATE TABLE createuser(
username VARCHAR(20),
loginname VARCHAR(20),
password VARCHAR(20),
conpassword VARCHAR(20),
role VARCHAR(10));

-- #Create procedure for createuser
CREATE PROCEDURE p_createuser
@username VARCHAR(20) = NULL,
@loginname VARCHAR(20) = NULL,
@password VARCHAR(20) = NULL,
@conpassword VARCHAR(20) = NULL,
@role VARCHAR(10) OUTPUT AS
BEGIN TRAN
	BEGIN TRY
	if @username IS NULL
	BEGIN 
	SET @username = @@IDENTITY
	INSERT INTO createuser (username, loginname, password, conpassword, role)
	VALUES (@username, @loginname, @password, @conpassword, @role)
	END
	ELSE
	BEGIN
		UPDATE createuser SET loginname=@loginname, password=@password, conpassword=@conpassword, role=@role
		WHERE username=@username
	END
	END TRY
	BEGIN CATCH
	ROLLBACK TRAN
		RETURN -1
	END CATCH
	COMMIT TRAN