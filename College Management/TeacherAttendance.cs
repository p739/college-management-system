﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace College_Management
{
    public partial class TeacherAttendance : Form
    {
        public TeacherAttendance()
        {
            InitializeComponent();
        }

        private Boolean valid()
        {
            if(txtTeacherName.Text == "")
            {
                MessageBox.Show("Enter Teacher Name");
                txtTeacherName.Focus();
                return false;
            }
            if(txtAttendance.Text == "")
            {
                MessageBox.Show("Enter Presenty");
                txtAttendance.Focus();
                return false;
            }
            return true;
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            txtTeacherName.Clear();
            txtAttendance.Clear();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if(valid())
            {
                // connection_string
                SqlConnection con = new SqlConnection(@"Data Source=BLACKTECH;Initial Catalog=CollegeManagement;Integra ted Security=True");

                try
                {
                    // #Logic : for_fordwording_data_from_txtbox_to_database_by_procedure
                    SqlCommand cmd = new SqlCommand();

                    cmd.Parameters.AddWithValue("@teachername", txtTeacherName.Text);
                    cmd.Parameters.AddWithValue("@attendance", txtAttendance.Text);
                    cmd.Parameters.AddWithValue("@attendancedate", attendance_date.Text.ToString());

                    //name _of_procedure_prescent_in_the_database
                    cmd.CommandText = "p_Teacher_Attendance";
                    cmd.CommandType = CommandType.StoredProcedure;

                    //getting_return_value_when_procedure_is_executed
                    SqlParameter rvalue = cmd.Parameters.Add("@r", SqlDbType.Int);
                    rvalue.Direction = ParameterDirection.ReturnValue;

                    //Execution_of_procedure
                    cmd.Connection = con;
                    con.Open();
                    cmd.ExecuteNonQuery();

                    int r = 0;
                    r = (int)rvalue.Value;

                    //for_success_the_data_enty_into_database
                    if (r > 0)
                    {
                        MessageBox.Show("Data Saved Successfully");

                        //#clearing_data_prescent_in_the_txtboxes
                        txtTeacherName.Clear();
                        txtAttendance.Clear();
                    }
                    else
                    {
                        //printing_Error_while_executiong_procedure_if_it 
                        MessageBox.Show("Something went wrong");
                    }
                }
                catch (Exception ex)
                {
                    //printing_error_while_executing_try_block
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    con.Close();
                }
            }
        }

        private void txtTeacherName_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) & (Keys)e.KeyChar != Keys.Back)
            {
                e.Handled = true;
            }
        }

        private void txtAttendance_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) & (Keys)e.KeyChar != Keys.Back)
            {
                e.Handled = true;
            }
        }
    }
}
