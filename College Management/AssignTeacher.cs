﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace College_Management
{
    public partial class AssignTeacher : Form
    {
        public AssignTeacher()
        {
            InitializeComponent();
        }

        private Boolean valid()
        {
            if(txtRegNo.Text == "")
            {
                MessageBox.Show("Enter Teacher Regestration Number");
                txtRegNo.Focus();
                return false;
            }
            if(txtTeacherName.Text == "")
            {
                MessageBox.Show("Enter Teacher Name");
                txtTeacherName.Focus();
                return false;
            }
            if(txtGender.Text == "")
            {
                MessageBox.Show("Enter Gender");
                txtGender.Focus();
                return false;
            }
            if(txtTeacherAddr.Text == "")
            {
                MessageBox.Show("Enter Teacher's Address");
                txtTeacherAddr.Focus();
                return false;
            }
            if(txtPhonNo.Text == "")
            {
                MessageBox.Show("Enter Teacher's Mobile Number");
                txtPhonNo.Focus();
                return false;
            }
            if(txtEmail.Text == "")
            {
                MessageBox.Show("Enter Email Id");
                txtEmail.Focus();
                return false;
            }
            if(txtExperience.Text == "")
            {
                MessageBox.Show("Enter Teacher's Experience");
                txtExperience.Focus();
                return false;
            }
            if(txtSepci.Text == "")
            {
                MessageBox.Show("Enter Teacher's Specilization");
                txtSepci.Focus();
                return false;
            }
            if(txtshift.Text == "")
            {
                MessageBox.Show("Enter Teacher's Shift");
                txtshift.Focus();
                return false;
            }
            return true;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            txtRegNo.Clear();
            txtTeacherName.Clear();
            txtGender.Clear();
            txtEmail.Clear();
            txtExperience.Clear();
            txtPhonNo.Clear();
            txtSepci.Clear();
            txtshift.Clear();
            txtTeacherAddr.Clear();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (valid())
            {
                //connection_string
                SqlConnection con = new SqlConnection(@"Data Source=BLACKTECH;Initial Catalog=CollegeManagement;Integra ted Security=True");

                try
                {
                    // #Logic : for_fordwording_data_from_txtbox_to_database_by_procedure
                    SqlCommand cmd = new SqlCommand();

                    if (txtRegNo.Text == "")
                    {
                        cmd.Parameters.AddWithValue("@regno", null);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@regno", txtRegNo.Text);
                    }
                    cmd.Parameters.AddWithValue("@teachername", txtTeacherName.Text);
                    cmd.Parameters.AddWithValue("@teacheraddr", txtTeacherAddr.Text);
                    cmd.Parameters.AddWithValue("@gender", txtGender.Text);
                    cmd.Parameters.AddWithValue("@email", txtEmail);
                    cmd.Parameters.AddWithValue("@experience", txtExperience.Text);
                    cmd.Parameters.AddWithValue("@phoneno", txtPhonNo.Text);
                    cmd.Parameters.AddWithValue("@speci", txtSepci.Text);
                    cmd.Parameters.AddWithValue("@shift", txtshift.Text);
                    cmd.Parameters.AddWithValue("@dob", date_of_birth.Text.ToString());
                    cmd.Parameters.AddWithValue("@ad", assign_date.Text.ToString());

                    //name _of_procedure_prescent_in_the_database
                    cmd.CommandText = "p_Assign_Teacher";
                    cmd.CommandType = CommandType.StoredProcedure;

                    //getting_return_value_when_procedure_is_executed
                    SqlParameter rvalue = cmd.Parameters.Add("@r", SqlDbType.Int);
                    rvalue.Direction = ParameterDirection.ReturnValue;

                    //Execution_of_procedure
                    cmd.Connection = con;
                    con.Open();
                    cmd.ExecuteNonQuery();

                    int r = 0;
                    r = (int)rvalue.Value;

                    //for_success_the_data_enty_into_database
                    if (r > 0)
                    {
                        MessageBox.Show("Data Updated Successfully");

                        //#clearing_data_prescent_in_the_txtboxes
                        txtRegNo.Clear();
                        txtTeacherName.Clear();
                        txtGender.Clear();
                        txtEmail.Clear();
                        txtExperience.Clear();
                        txtPhonNo.Clear();
                        txtSepci.Clear();
                        txtshift.Clear();
                        txtTeacherAddr.Clear();
                    }
                    else
                    {
                        //printing_Error_while_executiong_procedure_if_it 
                        MessageBox.Show("Something went wrong");
                    }
                }
                catch (Exception ex)
                {
                    //printing_error_while_executing_try_block
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    con.Close();
                }
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if(valid())
            {
                //connection_string
                SqlConnection con = new SqlConnection(@"Data Source=BLACKTECH;Initial Catalog=CollegeManagement;Integra ted Security=True");

                try
                {
                    // #Logic : for_fordwording_data_from_txtbox_to_database_by_procedure
                    SqlCommand cmd = new SqlCommand();

                    if(txtRegNo.Text == "")
                    {
                        cmd.Parameters.AddWithValue("@regno", null);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@regno", txtRegNo.Text);
                    }
                    cmd.Parameters.AddWithValue("@teachername", txtTeacherName.Text);
                    cmd.Parameters.AddWithValue("@teacheraddr", txtTeacherAddr.Text);
                    cmd.Parameters.AddWithValue("@gender", txtGender.Text);
                    cmd.Parameters.AddWithValue("@email", txtEmail);
                    cmd.Parameters.AddWithValue("@experience", txtExperience.Text);
                    cmd.Parameters.AddWithValue("@phoneno", txtPhonNo.Text);
                    cmd.Parameters.AddWithValue("@speci", txtSepci.Text);
                    cmd.Parameters.AddWithValue("@shift", txtshift.Text);
                    cmd.Parameters.AddWithValue("@dob", date_of_birth.Text.ToString());
                    cmd.Parameters.AddWithValue("@ad", assign_date.Text.ToString());

                    //name _of_procedure_prescent_in_the_database
                    cmd.CommandText = "p_Assign_Teacher";
                    cmd.CommandType = CommandType.StoredProcedure;

                    //getting_return_value_when_procedure_is_executed
                    SqlParameter rvalue = cmd.Parameters.Add("@r", SqlDbType.Int);
                    rvalue.Direction = ParameterDirection.ReturnValue;

                    //Execution_of_procedure
                    cmd.Connection = con;
                    con.Open();
                    cmd.ExecuteNonQuery();

                    int r = 0;
                    r = (int)rvalue.Value;

                    //for_success_the_data_enty_into_database
                    if (r > 0)
                    {
                        MessageBox.Show("Data Saved Successfully");

                        //#clearing_data_prescent_in_the_txtboxes
                        txtRegNo.Clear();
                        txtTeacherName.Clear();
                        txtGender.Clear();
                        txtEmail.Clear();
                        txtExperience.Clear();
                        txtPhonNo.Clear();
                        txtSepci.Clear();
                        txtshift.Clear();
                        txtTeacherAddr.Clear();
                    }
                    else
                    {
                        //printing_Error_while_executiong_procedure_if_it 
                        MessageBox.Show("Something went wrong");
                    }
                }
                catch (Exception ex)
                {
                    //printing_error_while_executing_try_block
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    con.Close();
                }
            }
        }

        private void txtRegNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) & (Keys)e.KeyChar != Keys.Back)
            {
                e.Handled = true;
            }
        }

        private void txtTeacherName_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) & (Keys)e.KeyChar != Keys.Back)
            {
                e.Handled = true;
            }
        }

        private void txtGender_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) & (Keys)e.KeyChar != Keys.Back)
            {
                e.Handled = true;
            }
        }

        private void txtPhonNo_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtPhonNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) & (Keys)e.KeyChar != Keys.Back)
            {
                e.Handled = true;
            }
        }

        private void txtExperience_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) & (Keys)e.KeyChar != Keys.Back)
            {
                e.Handled = true;
            }
        }

        private void txtSepci_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) & (Keys)e.KeyChar != Keys.Back)
            {
                e.Handled = true;
            }
        }

        private void txtshift_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) & (Keys)e.KeyChar != Keys.Back)
            {
                e.Handled = true;
            }
        }
    }
}
