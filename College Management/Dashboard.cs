﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace College_Management
{
    public partial class Dashboard : Form
    {
        public Dashboard()
        {
            InitializeComponent();
        }

        private void newAdmissionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StudentAdmission studadm = new StudentAdmission();
            studadm.ShowDialog();
        }

        private void feeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Fee f = new Fee();
            f.ShowDialog();
        }

        private void feeHistoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StudFeeView stf = new StudFeeView();
            stf.ShowDialog();
        }

        private void Dashboard_Load(object sender, EventArgs e)
        {

        }

        private void attandanceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Attendence at = new Attendence();
            at.ShowDialog();
        }

        private void attToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AttendenceHistory athis = new AttendenceHistory();
            athis.ShowDialog();
        }

        private void assignNewTeacherToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AssignTeacher AT = new AssignTeacher();
            AT.ShowDialog();
        }

        private void attendanceHistoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TeacherAttendanceHistory TAH = new TeacherAttendanceHistory();
            TAH.ShowDialog();
        }

        private void scheduleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TeacherTimeSchedule TTS = new TeacherTimeSchedule();
            TTS.ShowDialog();
        }

        private void teacherAttendanceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TeacherAttendance TA = new TeacherAttendance();
            TA.ShowDialog();
        }

        private void scheduleHistoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TeacherScheduleHistory TSH = new TeacherScheduleHistory();
            TSH.ShowDialog();
        }

        private void createAccountToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CreateUser CU = new CreateUser();
            CU.ShowDialog();
        }

        private void logOutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
