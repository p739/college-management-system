﻿namespace College_Management
{
    partial class TeacherTimeSchedule
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtPeriod = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtTimer = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.Schedule_date = new System.Windows.Forms.DateTimePicker();
            this.txtShift = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.Day = new System.Windows.Forms.Label();
            this.txtSubName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtSemister = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtCource = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtTeacherName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtPeriod);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.txtTimer);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.Schedule_date);
            this.groupBox1.Controls.Add(this.txtShift);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.Day);
            this.groupBox1.Controls.Add(this.txtSubName);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtSemister);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtCource);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtTeacherName);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(766, 288);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "[ Teacher Time Schedule ]";
            // 
            // txtPeriod
            // 
            this.txtPeriod.Location = new System.Drawing.Point(451, 235);
            this.txtPeriod.Name = "txtPeriod";
            this.txtPeriod.Size = new System.Drawing.Size(284, 27);
            this.txtPeriod.TabIndex = 49;
            this.txtPeriod.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPeriod_KeyPress);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(379, 235);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(51, 20);
            this.label6.TabIndex = 48;
            this.label6.Text = "Period";
            // 
            // txtTimer
            // 
            this.txtTimer.Location = new System.Drawing.Point(157, 232);
            this.txtTimer.Name = "txtTimer";
            this.txtTimer.Size = new System.Drawing.Size(197, 27);
            this.txtTimer.TabIndex = 47;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(18, 232);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(42, 20);
            this.label7.TabIndex = 46;
            this.label7.Text = "Time";
            // 
            // Schedule_date
            // 
            this.Schedule_date.Location = new System.Drawing.Point(157, 181);
            this.Schedule_date.Name = "Schedule_date";
            this.Schedule_date.Size = new System.Drawing.Size(197, 27);
            this.Schedule_date.TabIndex = 45;
            // 
            // txtShift
            // 
            this.txtShift.Location = new System.Drawing.Point(451, 184);
            this.txtShift.Name = "txtShift";
            this.txtShift.Size = new System.Drawing.Size(284, 27);
            this.txtShift.TabIndex = 11;
            this.txtShift.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtShift_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(379, 184);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(39, 20);
            this.label5.TabIndex = 10;
            this.label5.Text = "Shift";
            // 
            // Day
            // 
            this.Day.AutoSize = true;
            this.Day.Location = new System.Drawing.Point(18, 181);
            this.Day.Name = "Day";
            this.Day.Size = new System.Drawing.Size(35, 20);
            this.Day.TabIndex = 8;
            this.Day.Text = "Day";
            // 
            // txtSubName
            // 
            this.txtSubName.Location = new System.Drawing.Point(157, 132);
            this.txtSubName.Name = "txtSubName";
            this.txtSubName.Size = new System.Drawing.Size(578, 27);
            this.txtSubName.TabIndex = 7;
            this.txtSubName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSubName_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(18, 132);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(102, 20);
            this.label4.TabIndex = 6;
            this.label4.Text = "Subject Name";
            // 
            // txtSemister
            // 
            this.txtSemister.Location = new System.Drawing.Point(451, 87);
            this.txtSemister.Name = "txtSemister";
            this.txtSemister.Size = new System.Drawing.Size(284, 27);
            this.txtSemister.TabIndex = 5;
            this.txtSemister.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSemister_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(379, 87);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 20);
            this.label3.TabIndex = 4;
            this.label3.Text = "Semister";
            // 
            // txtCource
            // 
            this.txtCource.Location = new System.Drawing.Point(157, 84);
            this.txtCource.Name = "txtCource";
            this.txtCource.Size = new System.Drawing.Size(197, 27);
            this.txtCource.TabIndex = 3;
            this.txtCource.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCource_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 84);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 20);
            this.label2.TabIndex = 2;
            this.label2.Text = "Cource";
            // 
            // txtTeacherName
            // 
            this.txtTeacherName.Location = new System.Drawing.Point(157, 39);
            this.txtTeacherName.Name = "txtTeacherName";
            this.txtTeacherName.Size = new System.Drawing.Size(578, 27);
            this.txtTeacherName.TabIndex = 1;
            this.txtTeacherName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTeacherName_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Teacher Name";
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(262, 315);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(94, 29);
            this.btnAdd.TabIndex = 3;
            this.btnAdd.Text = "&Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnClear
            // 
            this.btnClear.CausesValidation = false;
            this.btnClear.Location = new System.Drawing.Point(406, 315);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(94, 29);
            this.btnClear.TabIndex = 4;
            this.btnClear.Text = "&Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // TeacherTimeSchedule
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 384);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.groupBox1);
            this.Name = "TeacherTimeSchedule";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Teacher Time Schedule";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private GroupBox groupBox1;
        private TextBox txtShift;
        private Label label5;
        private Label Day;
        private TextBox txtSubName;
        private Label label4;
        private TextBox txtSemister;
        private Label label3;
        private TextBox txtCource;
        private Label label2;
        private TextBox txtTeacherName;
        private Label label1;
        private DateTimePicker Schedule_date;
        private TextBox txtPeriod;
        private Label label6;
        private TextBox txtTimer;
        private Label label7;
        private Button btnAdd;
        private Button btnClear;
    }
}